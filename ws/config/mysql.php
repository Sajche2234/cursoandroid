<?php
function _conectDB(){
	$dbConfig["host"] = "localhost";
    $dbConfig["user"] = "intecap";
    $dbConfig["password"] = "Abc$2020";
    $dbConfig["db_name"] = "intecap";
	$strHost = (isset($dbConfig["host"]))?$dbConfig["host"]:"";
    $strUser = (isset($dbConfig["user"]))?$dbConfig["user"]:"";
    $strPass = (isset($dbConfig["password"]))?$dbConfig["password"]:"";
    $strDbName = (isset($dbConfig["db_name"]))?$dbConfig["db_name"]:"";
    if($strHost != "" && $strUser != "" && $strDbName != ""){
    	$link = @mysqli_connect($strHost, $strUser, $strPass, $strDbName);
    	 if (!$link) {
            print mysqli_connect_error();
            die("<br>Error al conectar a base de datos, por favor contacte a su administrador del sitio.");
        } else {
            @$link->set_charset("utf8");
        	return $link;
        }
    }
}

function _closeConectDB(){
    global $dbLink;
    mysqli_close($dbLink);
}

function _consulta($strQuery = ""){
    global $dbLink;    
    if($strQuery != ""){        
        $src = mysqli_query($dbLink, $strQuery);
        if($src){
            return $src;
        } else {
            @print  @mysqli_error($dbLink);
            return false;
        }
    }
}

function _assoc($qTMP = ""){
    if($qTMP){
        return mysqli_fetch_assoc($qTMP);
    } else {
        return false;
    }
}

function _number_rows($qTMP = ""){
    if($qTMP){
        return mysqli_num_rows($qTMP);
    } else {
        return false;
    }
}

function _lastID(){
    global $dbLink;    
    return mysqli_insert_id($dbLink);
}

function _escape($var = false){
    global $dbLink;    
    if($var){
        return mysqli_escape_string($dbLink, $var);   
    } else {
        return false;
    }
}

function _response($strQuery = "", $boolPrintResponse = false){    
    if($strQuery != ""){        
        if($boolPrintResponse){
            if(_do_query($strQuery)){
                print "ok";
            } else {
                print "fail";
            }
        } else {
            if(_do_query($strQuery)){
                return "ok";
            } else {
                return "fail";
            }
        }   
    }
}

function _freeResult($qTMP = false){
    global $dbLink;    
    if($qTMP){
        return mysqli_free_result($qTMP);   
    } else {
        return false;
    }
}
function depurar($strcontent = "") {
    if ($strcontent == "" || $strcontent == null || $strcontent == false) {
        var_dump($strcontent);
    } else {
        print_r("<pre>");
        print_r($strcontent);
        print_r("</pre>");
    }
}
?>