<?php
require_once '../config/main.php';
$jsonArray = array();
$query = "SELECT * FROM tbl_cliente ORDER BY id_cliente DESC";
$qTMP = _consulta($query);
while ( ($obj = _assoc($qTMP)) != NULL ) {
	$jsonArray["tbl_cliente"][]=$obj;
}
if (!empty($jsonArray)) {
	_closeConectDB();
} else {
	$resultadoVacio = array();
	$resultadoVacio["id_cliente"] = "";
	$resultadoVacio["nombre_cliente"] = "";
	$resultadoVacio["apellido_cliente"] = "";
	$resultadoVacio["telefono_cliente"] = "";
	$resultadoVacio["direccion_cliente"] = "";
	$jsonArray['tbl_cliente'][] = $resultadoVacio;
}
print json_encode($jsonArray);
?>