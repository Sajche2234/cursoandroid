<?php 
require_once '../config/main.php';
$jsonArray = array();
$error=false;
$cve_art = (isset($_REQUEST["cve_art"]))?$_REQUEST["cve_art"]:NULL;
$nombre_producto = (isset($_REQUEST["nombre_producto"]))?$_REQUEST["nombre_producto"]:NULL;
$stock_minimo = (isset($_REQUEST["stock_minimo"]))?$_REQUEST["stock_minimo"]:NULL;
$stock_maximo = (isset($_REQUEST["stock_maximo"]))?$_REQUEST["stock_maximo"]:NULL;
$precio_compra = (isset($_REQUEST["precio_compra"]))?$_REQUEST["precio_compra"]:NULL;
$precio_venta = (isset($_REQUEST["precio_venta"]))?$_REQUEST["precio_venta"]:NULL;
$estado = (isset($_REQUEST["estado"]))?$_REQUEST["estado"]:NULL;
if($cve_art==NULL) { $error=true; }
if($nombre_producto==NULL) { $error=true; } else { $nombre_producto = "'".$nombre_producto."'"; }
if($stock_minimo==NULL) { $error=true; } else { $stock_minimo = "'".$stock_minimo."'"; }
if($stock_maximo==NULL) { $error=true; } else { $stock_maximo = "'".$stock_maximo."'"; }
if($precio_compra==NULL) { $error=true; } else { $precio_compra = "'".$precio_compra."'"; }
if($precio_venta==NULL) { $error=true; } else { $precio_venta = "'".$precio_venta."'"; }
if($estado==NULL) { $error=true; } else { $estado = "'".$estado."'"; }
if($error==false){
	$query = "UPDATE tbl_producto SET nombre_producto=$nombre_producto, stock_minimo=$stock_minimo, stock_maximo=$stock_maximo, precio_compra=$precio_compra, precio_venta=$precio_venta, estado=$estado WHERE cve_art = $cve_art";
	$result = _consulta($query);
	$jsonArray['tbl_producto'] = $result;
	print json_encode($jsonArray);
	_closeConectDB();
} else {
	$jsonArray['tbl_producto'] = false;
	print json_encode($jsonArray);
	_closeConectDB();
	print "Datos no insertados";
}
?>