<?php
require_once '../config/main.php';
$jsonArray = array();
$error=false;
$cve_art	 = (isset($_REQUEST["cve_art"]))?$_REQUEST["cve_art"]:NULL;
if($cve_art == NULL) {
	$resultadoVacio = array();
	$resultadoVacio["cve_art"] = "";
	$resultadoVacio["nombre_producto"] = "";
	$resultadoVacio["stock_minimo"] = "";
	$resultadoVacio["stock_maximo"] = "";
	$resultadoVacio["precio_compra"] = "";
	$resultadoVacio["precio_venta"] = "";
	$resultadoVacio["estado"] = "";
	$jsonArray['tbl_producto'][] = $resultadoVacio;
	print json_encode($jsonArray);
	print "Datos no encontrados";
} else {
	$arrTMP = _assoc(_consulta("SELECT cve_art, nombre_producto, stock_minimo, stock_maximo, precio_compra, precio_venta, estado FROM tbl_producto WHERE cve_art = '{$cve_art}' OR nombre_producto LIKE '%{$cve_art}%' LIMIT 0,1"));
	if($arrTMP) {
		$jsonArray['tbl_producto'][] = $arrTMP;
	} else {
		$resultadoVacio = array();
		$resultadoVacio["cve_art"] = "";
		$resultadoVacio["nombre_producto"] = "";
		$resultadoVacio["stock_minimo"] = "";
		$resultadoVacio["stock_maximo"] = "";
		$resultadoVacio["precio_compra"] = "";
		$resultadoVacio["precio_venta"] = "";
		$resultadoVacio["estado"] = "";
		$jsonArray['tbl_producto'][] = $resultadoVacio;
	}
	print json_encode($jsonArray);
	_closeConectDB();
}
?>