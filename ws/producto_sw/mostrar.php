<?php
require_once '../config/main.php';
$jsonArray = array();
$query = "SELECT * FROM tbl_producto ORDER BY cve_art DESC";
$qTMP = _consulta($query);
while ( ($obj = _assoc($qTMP)) != NULL ) {
	$jsonArray["tbl_producto"][]=$obj;
}
if (!empty($jsonArray)) {
	_closeConectDB();
} else {
	$resultadoVacio = array();
	$resultadoVacio["cve_art"] = "";
	$resultadoVacio["nombre_producto"] = "";
	$resultadoVacio["stock_minimo"] = "";
	$resultadoVacio["stock_maximo"] = "";
	$resultadoVacio["precio_compra"] = "";
	$resultadoVacio["precio_venta"] = "";
	$resultadoVacio["estado"] = "";
	$jsonArray['tbl_producto'][] = $resultadoVacio;
}
print json_encode($jsonArray);
?>