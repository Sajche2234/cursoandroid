package com.robert.prodoctoswsrs;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.robert.prodoctoswsrs.complementos.MetodosWS;
import com.robert.prodoctoswsrs.complementos.ProductoVO;

import org.json.JSONArray;
import org.json.JSONObject;

public class EditActivityWS extends AppCompatActivity implements Response.Listener<JSONObject>, Response.ErrorListener {
    String cve_art;
    EditText edtNombre, edtSMin, edtSMax, edtPC, edtPV, edtEstado;
    MetodosWS metodosWS = new MetodosWS();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_ws);
        Bundle bundle = getIntent().getExtras();
        cve_art = bundle.getString("cve_art");
        edtNombre = findViewById(R.id.edtNombre);
        edtSMin = findViewById(R.id.edtSMin);
        edtSMax = findViewById(R.id.edtSMax);
        edtPC = findViewById(R.id.edtPC);
        edtPV = findViewById(R.id.edtPV);
        edtEstado = findViewById(R.id.edtEstado);
        this.loadData();
    }

    public void onClick(View view) {
        //Toast.makeText(this, "Actualizando producto. " + cve_art, Toast.LENGTH_LONG).show();
        this.actualizarP();
    }

    private void actualizarP(){
        String nombre, smin, smax, compra, venta, estado;
        nombre = edtNombre.getText().toString();
        smin = edtSMin.getText().toString();
        smax = edtSMax.getText().toString();
        compra = edtPC.getText().toString();
        venta = edtPV.getText().toString();
        estado = edtEstado.getText().toString();
        if(!cve_art.isEmpty() && !nombre.isEmpty() && !smin.isEmpty() && !smax.isEmpty() && !compra.isEmpty() && !venta.isEmpty() && !estado.isEmpty()){
            metodosWS.editarSW(this, Integer.parseInt(cve_art), nombre, Integer.parseInt(smin), Integer.parseInt(smax), Float.parseFloat(compra), Float.parseFloat(venta), estado, this, this);
            Toast.makeText(this, "El registro ha sido actualizado exitosamente", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            finish();
        } else {
            Toast.makeText(this, "Debe llenar el formulario completo.", Toast.LENGTH_LONG).show();
        }
    }

    private void loadData(){
        if(!cve_art.isEmpty()){
            metodosWS.buscarIDWS(this, cve_art, this, this);
        } else {
            Toast.makeText(this, "ERROR: No existe parámetros de búsquda.", Toast.LENGTH_LONG).show();
        }
    }

    private void resultadoConsulta(JSONObject response){
        ProductoVO productoVO = new ProductoVO();
        JSONArray jsonArray = response.optJSONArray("tbl_producto");
        try {
            JSONObject jsonObject = jsonArray.getJSONObject(0);
            //System.err.println(jsonObject);
            productoVO.setCve_art(jsonObject.getString("cve_art"));
            productoVO.setNombre_producto(jsonObject.optString("nombre_producto"));
            productoVO.setStock_minimo(jsonObject.optInt("stock_minimo"));
            productoVO.setStock_maximo(jsonObject.optInt("stock_maximo"));
            productoVO.setPrecio_compra(Float.parseFloat(jsonObject.optString("precio_compra")));
            productoVO.setPrecio_venta(Float.parseFloat(jsonObject.optString("precio_venta")));
            productoVO.setEstado(jsonObject.optString("estado"));
            edtNombre.setText("" + productoVO.getNombre_producto());
            edtSMin.setText("" + productoVO.getStock_minimo());
            edtSMax.setText("" + productoVO.getStock_maximo());
            edtPC.setText("" + productoVO.getPrecio_compra());
            edtPV.setText("" + productoVO.getPrecio_venta());
            edtEstado.setText("" + productoVO.getEstado());
        } catch (Exception e){
            Toast.makeText(this, "Error referente a Buscar en Editar...", Toast.LENGTH_LONG).show();
            System.err.println("BiE------ " + e.getCause() + " ----- " + e.getMessage());
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        this.resultadoConsulta(response);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(this, "Error al response buscar datos en Buscar. " + error, Toast.LENGTH_LONG).show();
        System.err.println("BiEd------ " + error);
        edtNombre.setText("");
        edtSMin.setText("");
        edtSMax.setText("");
        edtPC.setText("");
        edtPV.setText("");
        edtEstado.setText("");
    }
}