package com.robert.prodoctoswsrs;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.robert.prodoctoswsrs.complementos.MetodosWS;
import com.robert.prodoctoswsrs.complementos.ProductoVO;

import org.json.JSONArray;
import org.json.JSONObject;

public class VistaActivityWS extends AppCompatActivity implements Response.Listener<JSONObject>, Response.ErrorListener {
    String cve_art;
    TextView txtNombre, txtSMin, txtSMax, txtPrecioC, txtPrecioV, txtEstado;
    MetodosWS metodosWS = new MetodosWS();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vista_ws);
        Bundle bundle = getIntent().getExtras();
        cve_art = bundle.getString("cve_art");
        txtNombre = findViewById(R.id.txtNombre);
        txtSMin = findViewById(R.id.txtSMin);
        txtSMax = findViewById(R.id.txtSMax);
        txtPrecioC = findViewById(R.id.txtPrecioC);
        txtPrecioV = findViewById(R.id.txtPrecioV);
        txtEstado = findViewById(R.id.txtEstado);
        this.loadData();
    }

    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnEdit:
                Intent intent = new Intent(getApplicationContext(), EditActivityWS.class);
                intent.putExtra("cve_art", cve_art);
                startActivity(intent);
                finish();
                break;
            case R.id.btnDelete:

                break;
        }
    }
    private void loadData(){
        if(!cve_art.isEmpty()){
            metodosWS.buscarIDWS(this, cve_art, this, this);
        } else {
            Toast.makeText(this, "ERROR: No existe parámetros de búsquda.", Toast.LENGTH_LONG).show();
        }
    }

    private void eliminarP(){
        if(!cve_art.isEmpty()){
            metodosWS.eliminarSW(this, Integer.parseInt(cve_art), this, this);
            Toast.makeText(this, "Datos eliminados correctamente.", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            finish();
        } else {
            Toast.makeText(this, "Debe ingresar un codigo.", Toast.LENGTH_LONG).show();
        }
    }

    private void resultadoConsulta(JSONObject response){
        ProductoVO productoVO = new ProductoVO();
        JSONArray jsonArray = response.optJSONArray("tbl_producto");
        try {
            JSONObject jsonObject = jsonArray.getJSONObject(0);
            //System.err.println(jsonObject);
            productoVO.setCve_art(jsonObject.getString("cve_art"));
            productoVO.setNombre_producto(jsonObject.optString("nombre_producto"));
            productoVO.setStock_minimo(jsonObject.optInt("stock_minimo"));
            productoVO.setStock_maximo(jsonObject.optInt("stock_maximo"));
            productoVO.setPrecio_compra(Float.parseFloat(jsonObject.optString("precio_compra")));
            productoVO.setPrecio_venta(Float.parseFloat(jsonObject.optString("precio_venta")));
            productoVO.setEstado(jsonObject.optString("estado"));
            txtNombre.setText("" + productoVO.getNombre_producto());
            txtSMin.setText("" + productoVO.getStock_minimo());
            txtSMax.setText("" + productoVO.getStock_maximo());
            txtPrecioC.setText("" + productoVO.getPrecio_compra());
            txtPrecioV.setText("" + productoVO.getPrecio_venta());
            txtEstado.setText("" + productoVO.getEstado());
        } catch (Exception e){
            Toast.makeText(this, "Error referente a Buscar...", Toast.LENGTH_LONG).show();
            System.err.println("B------ " + e.getCause() + " ----- " + e.getMessage());
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        this.resultadoConsulta(response);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(this, "Error al response buscar datos. " + error, Toast.LENGTH_LONG).show();
        System.err.println("B------ " + error);
        txtNombre.setText("");
        txtSMin.setText("");
        txtSMax.setText("");
        txtPrecioC.setText("");
        txtPrecioV.setText("");
        txtEstado.setText("");
    }
}