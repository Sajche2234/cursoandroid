package com.robert.prodoctoswsrs;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.robert.prodoctoswsrs.complementos.MetodosWS;

import org.json.JSONObject;

public class NewActivityWS extends AppCompatActivity implements Response.Listener<JSONObject>, Response.ErrorListener {
    EditText edtNombre, edtSMin, edtSMax, edtPC, edtPV, edtEstado;
    MetodosWS metodosWS = new MetodosWS();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_ws);
        edtNombre = findViewById(R.id.edtNombre);
        edtSMin = findViewById(R.id.edtSMin);
        edtSMax = findViewById(R.id.edtSMax);
        edtPC = findViewById(R.id.edtPC);
        edtPV = findViewById(R.id.edtPV);
        edtEstado = findViewById(R.id.edtEstado);
    }

    public void onClick(View view) {
        this.insertar();
    }

    private void insertar(){
        String nombre, smin, smax, compra, venta, estado;
        nombre = edtNombre.getText().toString();
        smin = edtSMin.getText().toString();
        smax = edtSMax.getText().toString();
        compra = edtPC.getText().toString();
        venta = edtPV.getText().toString();
        estado = edtEstado.getText().toString();
        if(!nombre.isEmpty() && !smin.isEmpty() && !smax.isEmpty() && !compra.isEmpty() && !venta.isEmpty() && !estado.isEmpty()){
            metodosWS.insertarWS(this, nombre, Integer.parseInt(smin), Integer.parseInt(smax), Float.parseFloat(compra), Float.parseFloat(venta), estado, this, this);
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        } else {
            Toast.makeText(this, "Debe llenar el formulario completo.", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        Toast.makeText(this, "Datos insertados correctamente", Toast.LENGTH_LONG).show();
        this.finish();
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(this, "Error al insertados datos. " + error, Toast.LENGTH_LONG).show();
        System.err.println("I------ " + error);
    }
}