package com.robert.prodoctoswsrs;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.robert.prodoctoswsrs.complementos.MetodosWS;
import com.robert.prodoctoswsrs.complementos.ProductoVO;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements Response.Listener<JSONObject>, Response.ErrorListener {
    ListView listView;
    ArrayList<String> listaDatos;
    ArrayList<ProductoVO> listaProductoVO;
    EditText edtBuscar;
    MetodosWS metodosSW = new MetodosWS();
    String buscar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = findViewById(R.id.lvListaProductos);
        edtBuscar = findViewById(R.id.edtBuscar);
        buscar = edtBuscar.getText().toString();
        metodosSW.consultarWS(this, buscar, this, this);
    }

    public void onClick(View view) {
        Intent intent;
        switch(view.getId()) {
            case R.id.btnNuevo:
                intent = new Intent(this, NewActivityWS.class);
                startActivity(intent);
                break;
            case R.id.btnRefrescar:
                buscar = edtBuscar.getText().toString();
                metodosSW.consultarWS(this, buscar, this, this);
                break;
        }
    }

    private void listarContenido(JSONObject response){
        JSONArray jsonArray = response.optJSONArray("tbl_producto");
        listaProductoVO = new ArrayList<>();
        try {
            for(int i=0; i < jsonArray.length(); i++){
                ProductoVO productoVO = new ProductoVO();
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                //System.out.println(jsonObject);
                productoVO.setCve_art(jsonObject.optString("cve_art"));
                productoVO.setNombre_producto(jsonObject.optString("nombre_producto"));
                productoVO.setStock_minimo(jsonObject.optInt("stock_minimo"));
                productoVO.setStock_maximo(jsonObject.optInt("stock_maximo"));
                productoVO.setPrecio_compra(Float.parseFloat(jsonObject.optString("precio_compra")));
                productoVO.setPrecio_venta(Float.parseFloat(jsonObject.optString("precio_venta")));
                productoVO.setEstado(jsonObject.optString("estado"));
                listaProductoVO.add(productoVO);
            }
            listaDatos = new ArrayList<>();
            for(int i=0; i < listaProductoVO.size(); i++){
                listaDatos.add(listaProductoVO.get(i).getCve_art() + ". " + listaProductoVO.get(i).getNombre_producto());
            }
            ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, listaDatos);
            listView.setAdapter(arrayAdapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    String codigo = listaProductoVO.get(i).getCve_art();
                    Intent intent = new Intent(getApplicationContext(), VistaActivityWS.class);
                    intent.putExtra("cve_art", codigo);
                    startActivity(intent);
                    //finish();
                }
            });
        } catch (Exception e){
            Toast.makeText(this, "Error referente a Mostrar...", Toast.LENGTH_LONG).show();
            System.err.println("M------ " + e.getCause() + " ----- " + e.getMessage());
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        this.listarContenido(response);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(this, "Error al response mostrar datos... " + error, Toast.LENGTH_LONG).show();
        System.err.println("B------ " + error);
    }
}