package com.edutec.myrecyclerview2021713;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class RecyclerActivity extends AppCompatActivity {
    ArrayList<Integer> imagenes = new ArrayList<>();
    RecyclerView recyclerView;
    TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);
        recyclerView = findViewById(R.id.recyclerId);
        textView = findViewById(R.id.txttitulo);
        Bundle bundle = getIntent().getExtras();
        int numero = bundle.getInt("codigo");
        if(numero==1){
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        }
        String titulo = bundle.getString("titulo");
        textView.setText(titulo);
        //Toast.makeText(this, "No. " + numero + ". Titulo " + titulo, Toast.LENGTH_LONG).show();
        imagenes.add(R.drawable.ic_img_1);
        imagenes.add(R.drawable.ic_img_2);
        imagenes.add(R.drawable.ic_img_3);
        imagenes.add(R.drawable.ic_img_4);
        imagenes.add(R.drawable.ic_img_5);
        imagenes.add(R.drawable.ic_img_6);
        imagenes.add(R.drawable.ic_img_7);
        imagenes.add(R.drawable.ic_img_8);
        imagenes.add(R.drawable.ic_img_9);
        imagenes.add(R.drawable.ic_img_10);
        imagenes.add(R.drawable.ic_img_11);
        imagenes.add(R.drawable.ic_img_12);
        AdaptadorRecycler adaptadorRecycler = new AdaptadorRecycler(imagenes);
        recyclerView.setAdapter(adaptadorRecycler);
    }
}