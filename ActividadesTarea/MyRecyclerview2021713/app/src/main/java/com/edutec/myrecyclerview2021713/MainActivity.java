package com.edutec.myrecyclerview2021713;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Menú");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.item1:
                Toast.makeText(this, "Listado", Toast.LENGTH_LONG).show();
                Intent i = new Intent(getApplicationContext(), RecyclerActivity.class);
                i.putExtra("codigo", 1);
                i.putExtra("titulo", "LISTA");
                startActivity(i);
                break;
            case R.id.item2:
                Toast.makeText(this, "Mosaico", Toast.LENGTH_SHORT).show();
                Intent j = new Intent(getApplicationContext(), RecyclerActivity.class);
                j.putExtra("codigo", 2);
                j.putExtra("titulo", "MOSAICO");
                startActivity(j);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}