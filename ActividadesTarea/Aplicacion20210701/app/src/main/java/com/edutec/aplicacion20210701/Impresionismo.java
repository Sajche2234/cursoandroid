package com.edutec.aplicacion20210701;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class Impresionismo extends AppCompatActivity {
    private ImageView imgTC1, imgTC2, imgTC3;
    private TextView txtNoTC1, txtDescTC1, txtPrTC1, txtNoTC2, txtDescTC2, txtPrTC2, txtNoTC3, txtDescTC3, txtPrTC3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_impresionismo);
        imgTC1 = findViewById(R.id.imgTC1);
        txtNoTC1 = findViewById(R.id.txtNoTC1);
        txtDescTC1 = findViewById(R.id.txtDescTC1);
        txtPrTC1 = findViewById(R.id.txtPrTC1);
        imgTC2 = findViewById(R.id.imgTC2);
        txtNoTC2 = findViewById(R.id.txtNoTC2);
        txtDescTC2 = findViewById(R.id.txtDescTC2);
        txtPrTC2 = findViewById(R.id.txtPrTC2);
        imgTC3 = findViewById(R.id.imgTC3);
        txtNoTC3 = findViewById(R.id.txtNoTC3);
        txtDescTC3 = findViewById(R.id.txtDescTC3);
        txtPrTC3 = findViewById(R.id.txtPrTC3);
    }

    public void naturaleza(View view) {
        String getNombre = txtNoTC1.getText().toString();
        String getPrecio = txtPrTC1.getText().toString();
        String getDesc = txtDescTC1.getText().toString();
        Intent intento = new Intent(this, Carrito.class);
        intento.putExtra("setNombre", getNombre);
        intento.putExtra("setPrecio", getPrecio);
        intento.putExtra("setDesc", getDesc);
        intento.putExtra("setImg",R.drawable.imagen04);
        startActivity(intento);
    }

    public void batalla(View view) {
        String getNombre = txtNoTC2.getText().toString();
        String getPrecio = txtPrTC2.getText().toString();
        String getDesc = txtDescTC2.getText().toString();
        Intent intento = new Intent(this, Carrito.class);
        intento.putExtra("setNombre", getNombre);
        intento.putExtra("setPrecio", getPrecio);
        intento.putExtra("setDesc", getDesc);
        intento.putExtra("setImg",R.drawable.imagen05);
        startActivity(intento);
    }

    public void niebla(View view) {
        String getNombre = txtNoTC3.getText().toString();
        String getPrecio = txtPrTC3.getText().toString();
        String getDesc = txtDescTC3.getText().toString();
        Intent intento = new Intent(this, Carrito.class);
        intento.putExtra("setNombre", getNombre);
        intento.putExtra("setPrecio", getPrecio);
        intento.putExtra("setDesc", getDesc);
        intento.putExtra("setImg",R.drawable.imagen06);
        startActivity(intento);
    }
}