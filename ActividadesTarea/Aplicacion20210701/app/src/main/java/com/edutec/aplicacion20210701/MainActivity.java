package com.edutec.aplicacion20210701;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private ListView lista;
    private Toolbar toolbar;
    ArrayList<String> categorias = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lista =findViewById(R.id.listaCategorias);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Menu");
        /*categorias.add(String.valueOf(R.string.categoria1));
        categorias.add(String.valueOf(R.string.categoria2));
        categorias.add(String.valueOf(R.string.categoria3));*/
        categorias.add("Arte Abstracto");
        categorias.add("Impresionismo");
        categorias.add("Subrealismo");
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, categorias);
        lista.setAdapter(arrayAdapter);
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                System.out.println(position);
                switch (position) {
                    case 0:
                        Intent intent = new Intent(getApplicationContext(), Abstracto.class);
                        startActivity(intent);
                        //Toast.makeText(getApplicationContext(), "Abstracto 1 " + position, Toast.LENGTH_SHORT).show();
                        break;
                    case 1:
                        Intent intent2 = new Intent(getApplicationContext(), Impresionismo.class);
                        startActivity(intent2);
                        //Toast.makeText(getApplicationContext(), "Abstracto 2 " + position, Toast.LENGTH_SHORT).show();
                        break;
                    case 2:
                        Intent intent3 = new Intent(getApplicationContext(), Subrealista.class);
                        startActivity(intent3);
                        //Toast.makeText(getApplicationContext(), "Abstracto 3 " + position, Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        break;
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.item1:
                Toast.makeText(this, "Hola, soy el item 1", Toast.LENGTH_SHORT).show();
                break;
            case R.id.item2:
                Toast.makeText(this, "Hola, soy el item 2", Toast.LENGTH_SHORT).show();
                break;
            case R.id.item3:
                Toast.makeText(this, "Hola, soy el item 3", Toast.LENGTH_SHORT).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}