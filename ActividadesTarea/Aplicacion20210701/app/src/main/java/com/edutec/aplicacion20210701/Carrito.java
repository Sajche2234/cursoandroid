package com.edutec.aplicacion20210701;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Carrito extends AppCompatActivity {
    private TextView txtName, txtPrice, txtDesc;
    private EditText txtNombreP, txtApellidoP, txtNitP;
    private ImageView imgID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carrito);
        txtName = findViewById(R.id.txtName);
        txtPrice = findViewById(R.id.txtPrice);
        txtDesc = findViewById(R.id.txtDesc);
        imgID = findViewById(R.id.imgID);
        txtNombreP = findViewById(R.id.txtNombreP);
        txtApellidoP = findViewById(R.id.txtApellidoP);
        txtNitP = findViewById(R.id.txtNitP);
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            txtName.setText(bundle.getString("setNombre"));
            txtPrice.setText(bundle.getString("setPrecio"));
            txtDesc.setText(bundle.getString("setDesc"));
            imgID.setImageResource(bundle.getInt("setImg"));
        } else {
            Toast.makeText(this, "Error al intentar cargar los datos en la actividad anterior.", Toast.LENGTH_SHORT).show();
        }
    }

    public void candelar(View view) {
        this.finish();
    }

    public void comprar(View view) {
        String precio = txtPrice.getText().toString();
        String nombreProducto = txtName.getText().toString();
        String nombre = txtNombreP.getText().toString();
        String apellido = txtApellidoP.getText().toString();
        String nit = txtNitP.getText().toString();
        if((!nombreProducto.isEmpty()) && (!nombre.isEmpty()) && (!apellido.isEmpty()) && (!nit.isEmpty())){
            Intent intento = new Intent(this, Compra.class);
            intento.putExtra("producto", nombreProducto);
            intento.putExtra("nombre", nombre);
            intento.putExtra("apellido", apellido);
            intento.putExtra("nit", nit);
            intento.putExtra("precio", precio);
            startActivity(intento);
        } else {
            Toast.makeText(this, "Debe de ingresar los datos en el formulario correspondiente.", Toast.LENGTH_SHORT).show();
        }
    }
}