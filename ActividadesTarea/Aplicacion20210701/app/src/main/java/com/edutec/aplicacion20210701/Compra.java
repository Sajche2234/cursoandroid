package com.edutec.aplicacion20210701;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class Compra extends AppCompatActivity {
    private TextView txtValorImpuesto, txtValor, txtNit, txtApellido, txtNombre, txtProducto;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compra);
        txtProducto = findViewById(R.id.txtProducto);
        txtNombre = findViewById(R.id.txtNombre);
        txtApellido = findViewById(R.id.txtApellido);
        txtNit = findViewById(R.id.txtNit);
        txtValor = findViewById(R.id.txtValor);
        txtValorImpuesto = findViewById(R.id.txtValorImpuesto);
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            txtProducto.setText(bundle.getString("producto"));
            txtNombre.setText(bundle.getString("nombre"));
            txtApellido.setText(bundle.getString("apellido"));
            txtNit.setText(bundle.getString("nit"));
            txtValor.setText(String.valueOf(bundle.getString("precio")));
            double precio = Double.parseDouble(bundle.getString("precio"));
            double total = (precio * 0.12) + precio;
            txtValorImpuesto.setText(""+total);
        } else {
            Toast.makeText(this, "Error al intentar obtener los datos.", Toast.LENGTH_SHORT).show();
        }
    }

    public void volver(View view) {
        finish();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}