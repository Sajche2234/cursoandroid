package com.edutec.actividadpaises;

public class Pais {
    int Imagen;
    String nombre;
    int poblacion;

    public Pais(int imagen, String nombre, int poblacion) {
        Imagen = imagen;
        this.nombre = nombre;
        this.poblacion = poblacion;
    }

    public int getImagen() {
        return Imagen;
    }

    public void setImagen(int imagen) {
        Imagen = imagen;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPoblacion() {
        return poblacion;
    }

    public void setPoblacion(int poblacion) {
        this.poblacion = poblacion;
    }
}
