package com.edutec.actividadpaises;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

public class PaisAdapter extends ArrayAdapter<Pais> {
    private Context context;
    private int resource;
    public PaisAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Pais> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        convertView = layoutInflater.inflate(resource, parent, false);
        ImageView imageView = convertView.findViewById(R.id.imgLista);
        TextView nombre = convertView.findViewById(R.id.txtNombre);
        TextView poblacion = convertView.findViewById(R.id.txtPoblacion);
        imageView.setImageResource(getItem(position).getImagen());
        nombre.setText(getItem(position).getNombre());
        poblacion.setText(""+getItem(position).getPoblacion());
        Button btn = convertView.findViewById(R.id.btnDetalle);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), Profile.class);
                i.putExtra("nombre", getItem(position).getNombre());
                i.putExtra("poblac", getItem(position).getPoblacion());
                i.putExtra("imagen", getItem(position).getImagen());
                v.getContext().startActivity(i);
                //Toast.makeText(v.getContext(), "Hola " + getItem(position).getPoblacion(), Toast.LENGTH_SHORT).show();
            }
        });
        return convertView;
    }
}
