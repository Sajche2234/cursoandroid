package com.edutec.actividadpaises;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class AdaptadorLista extends BaseAdapter {
    private Context context;
    private int referenciaLista;
    private ArrayList<String> paises = new ArrayList<>();
    private ArrayList<Integer> poblaciones = new ArrayList<>();
    ArrayList<Integer> flags = new ArrayList<>();

    public AdaptadorLista(Context context, int referenciaLista, ArrayList<String> paises, ArrayList<Integer> poblaciones, ArrayList<Integer> flags) {
        this.context = context;
        this.referenciaLista = referenciaLista;
        this.paises = paises;
        this.poblaciones = poblaciones;
        this.flags = flags;
    }

    @Override
    public int getCount() {
        return this.paises.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        LayoutInflater layoutInflater = LayoutInflater.from(this.context);
        v = layoutInflater.inflate(R.layout.lista_personalizada, null);
        String pais;
        pais = this.paises.get(position);
        TextView campoNombre;
        campoNombre = v.findViewById(R.id.txtNombre);
        campoNombre.setText(String.valueOf(pais));
        int poblacion = this.poblaciones.get(position);
        TextView campoPoblacion;
        campoPoblacion = v.findViewById(R.id.txtPoblacion);
        campoPoblacion.setText(""+poblacion);
        Button btn = v.findViewById(R.id.btnDetalle);
        int img = this.flags.get(position);
        ImageView imagen;
        imagen = v.findViewById(R.id.imgLista);
        imagen.setImageLevel(img);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Hola " + position);
                Toast.makeText(v.getContext(), "Hola " + position, Toast.LENGTH_SHORT).show();
            }
        });
        return v;
    }
}
