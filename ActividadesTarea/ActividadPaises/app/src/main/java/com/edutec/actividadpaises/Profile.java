package com.edutec.actividadpaises;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class Profile extends AppCompatActivity {
    private ImageView img;
    private TextView nombre, poblacion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        img = findViewById(R.id.imagen);
        nombre = findViewById(R.id.txtPNombre);
        poblacion = findViewById(R.id.txtPPoblacion);
        Bundle bundle = getIntent().getExtras();
        String txtname = bundle.getString("nombre");
        String txtpo = String.valueOf(bundle.getInt("poblac"));
        int imagen = bundle.getInt("imagen");
        nombre.setText(txtname);
        poblacion.setText(""+txtpo);
        img.setImageResource(imagen);
    }
}