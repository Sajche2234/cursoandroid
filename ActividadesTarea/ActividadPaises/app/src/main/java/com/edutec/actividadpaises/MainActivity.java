package com.edutec.actividadpaises;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    private ListView lista;
    ArrayList<String> paises = new ArrayList<>();
    ArrayList<Integer> poblaciones = new ArrayList<>();
    ArrayList<Integer> flags = new ArrayList<>();
    ArrayList<Pais> arrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Random aleatorio = new Random(System.currentTimeMillis());
        lista = findViewById(R.id.listaPaises);
        /*paises.add("Nueva Zelanda");
        paises.add("Canadá");
        paises.add("Los Estados Unidos");
        paises.add("Irlanda");
        paises.add("Alemania");
        paises.add("Los Países Bajos");
        paises.add("Dinamarca");
        paises.add("Suiza");
        paises.add("Australia");
        paises.add("Noruega");
        poblaciones.add(aleatorio.nextInt(1000000));
        poblaciones.add(aleatorio.nextInt(1000000));
        poblaciones.add(aleatorio.nextInt(1000000));
        poblaciones.add(aleatorio.nextInt(1000000));
        poblaciones.add(aleatorio.nextInt(1000000));
        poblaciones.add(aleatorio.nextInt(1000000));
        poblaciones.add(aleatorio.nextInt(1000000));
        poblaciones.add(aleatorio.nextInt(1000000));
        poblaciones.add(aleatorio.nextInt(1000000));
        poblaciones.add(aleatorio.nextInt(1000000));
        flags.add(R.drawable.newzealand);
        flags.add(R.drawable.newzealand);
        flags.add(R.drawable.newzealand);
        flags.add(R.drawable.newzealand);
        flags.add(R.drawable.newzealand);
        flags.add(R.drawable.newzealand);
        flags.add(R.drawable.newzealand);
        flags.add(R.drawable.newzealand);
        flags.add(R.drawable.newzealand);
        flags.add(R.drawable.newzealand);
        AdaptadorLista adaptadorLista = new AdaptadorLista(this, R.layout.lista_personalizada, paises, poblaciones, flags);
        lista.setAdapter(adaptadorLista);*/
        arrayList.add(new Pais(R.drawable.newzealand, "Nueva Zelanda", aleatorio.nextInt(1000000)));
        arrayList.add(new Pais(R.drawable.canada, "Canada", aleatorio.nextInt(1000000)));
        arrayList.add(new Pais(R.drawable.imgusa, "Los Estados Unidos", aleatorio.nextInt(1000000)));
        arrayList.add(new Pais(R.drawable.ireland, "Irlanda", aleatorio.nextInt(1000000)));
        arrayList.add(new Pais(R.drawable.germany, "Alemania", aleatorio.nextInt(1000000)));
        arrayList.add(new Pais(R.drawable.imagetnetherlands, "Los Paises Bajos", aleatorio.nextInt(1000000)));
        arrayList.add(new Pais(R.drawable.denmark, "Dinamarca", aleatorio.nextInt(1000000)));
        arrayList.add(new Pais(R.drawable.switzerland, "Suiza", aleatorio.nextInt(1000000)));
        arrayList.add(new Pais(R.drawable.australia, "Australia", aleatorio.nextInt(1000000)));
        arrayList.add(new Pais(R.drawable.noruega, "Noruega", aleatorio.nextInt(1000000)));
        // Creando el adaptador
        PaisAdapter paisAdapter = new PaisAdapter(this, R.layout.lista_personalizada, arrayList);
        lista.setAdapter(paisAdapter);
    }
}