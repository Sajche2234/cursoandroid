package com.edutec.actividad202107061;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.os.Bundle;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ColorActivity extends AppCompatActivity {
    RelativeLayout relativeLayout;
    TextView codigoHexa;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_color);
        relativeLayout = findViewById(R.id.contenedorColor);
        codigoHexa = findViewById(R.id.txtHexadecimal);
        dato();
    }
    private void dato(){
        Bundle bundle = getIntent().getExtras();
        int color = bundle.getInt("color");
        String hexa = bundle.getString("codigoH");
        this.codigoHexa.setText(String.valueOf(hexa));
        this.relativeLayout.setBackgroundColor(ContextCompat.getColor(this, color));
    }
}