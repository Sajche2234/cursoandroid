package com.edutec.actividad202107061;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    //LinearLayout linearLayout;
    private Toolbar toolbar;
    private GridView gridView;
    ArrayList<String> nombres = new ArrayList<>();
    ArrayList<Integer> colores = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //linearLayout = findViewById(R.id.contenedor);
        //linearLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.colorRojo1));
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Menu");
        gridView = findViewById(R.id.gridView);
        nombres.add("#FFFFFF");
        nombres.add("#7B241C");
        nombres.add("#B03A2E");
        nombres.add("#633974");
        nombres.add("#5B2C6F");
        nombres.add("#1F618D");
        nombres.add("#2874A6");
        nombres.add("#117864");
        nombres.add("#0E6655");
        nombres.add("#1E8449");
        nombres.add("#F1C40F");
        nombres.add("#F39C12");
        nombres.add("#CA6F1E");
        nombres.add("#D35400");
        nombres.add("#7B7D7D");
        colores.add(R.color.color1);
        colores.add(R.color.color2);
        colores.add(R.color.color3);
        colores.add(R.color.color4);
        colores.add(R.color.color5);
        colores.add(R.color.color6);
        colores.add(R.color.color7);
        colores.add(R.color.color8);
        colores.add(R.color.color9);
        colores.add(R.color.color10);
        colores.add(R.color.color11);
        colores.add(R.color.color12);
        colores.add(R.color.color13);
        colores.add(R.color.color14);
        colores.add(R.color.color15);
        AdaptadorGrid adaptadorGrid = new AdaptadorGrid(this, R.layout.grid_personalizado, nombres, colores);
        gridView.setAdapter(adaptadorGrid);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(MainActivity.this, "A pulsado " + nombres.get(position), Toast.LENGTH_SHORT).show();
                Intent j = new Intent(getApplicationContext(), ColorActivity.class);
                j.putExtra("color", colores.get(position));
                j.putExtra("codigoH", nombres.get(position));
                startActivity(j);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.item1:
                Toast.makeText(this, "Hola, soy el item 1", Toast.LENGTH_SHORT).show();
                break;
            case R.id.item2:
                Toast.makeText(this, "Hola, soy el item 2", Toast.LENGTH_SHORT).show();
                break;
            case R.id.item3:
                Toast.makeText(this, "Hola, soy el item 3", Toast.LENGTH_SHORT).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}