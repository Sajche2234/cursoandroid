package com.edutec.actividad202107061;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import java.util.ArrayList;

public class AdaptadorGrid extends BaseAdapter {
    private Context context;
    private int referencia;
    private ArrayList<String> hexa = new ArrayList<>();
    private ArrayList<Integer> colores = new ArrayList<>();

    public AdaptadorGrid(Context context, int referencia, ArrayList<String> hexa, ArrayList<Integer> colores) {
        this.context = context;
        this.referencia = referencia;
        this.hexa = hexa;
        this.colores = colores;
    }

    @Override
    public int getCount() {
        return hexa.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        LayoutInflater layoutInflater = LayoutInflater.from(this.context);
        v = layoutInflater.inflate(R.layout.grid_personalizado, null);
        String hexad =this.hexa.get(position);
        int color = this.colores.get(position);
        TextView campoNombre;
        ImageView campoColor;
        campoNombre = v.findViewById(R.id.txtHexadecimal);
        campoColor = v.findViewById(R.id.imgGrid);
        campoNombre.setText(String.valueOf(hexad));
        campoColor.setBackgroundColor(ContextCompat.getColor(this.context, color));
        return v;
    }
}
