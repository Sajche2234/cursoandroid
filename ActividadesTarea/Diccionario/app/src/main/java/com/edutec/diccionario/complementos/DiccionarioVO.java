package com.edutec.diccionario.complementos;

public class DiccionarioVO {
    private int id;
    private String palabra, significado;

    public DiccionarioVO() {
    }

    public DiccionarioVO(int id, String palabra, String significado) {
        this.id = id;
        this.palabra = palabra;
        this.significado = significado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPalabra() {
        return palabra;
    }

    public void setPalabra(String palabra) {
        this.palabra = palabra;
    }

    public String getSignificado() {
        return significado;
    }

    public void setSignificado(String significado) {
        this.significado = significado;
    }
}
