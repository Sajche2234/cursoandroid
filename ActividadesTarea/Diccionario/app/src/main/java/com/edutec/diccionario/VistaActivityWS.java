package com.edutec.diccionario;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.edutec.diccionario.complementos.DiccionarioVO;
import com.edutec.diccionario.complementos.MetodosWS;

import org.json.JSONArray;
import org.json.JSONObject;

public class VistaActivityWS extends AppCompatActivity implements Response.Listener<JSONObject>, Response.ErrorListener {
    int codigo=0;
    TextView txtPalabra, txtSignificado;
    MetodosWS metodosWS = new MetodosWS();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vista_ws);
        txtPalabra = findViewById(R.id.txtPalabra);
        txtSignificado = findViewById(R.id.txtSignificado);
        Bundle bundle = getIntent().getExtras();
        codigo = bundle.getInt("codigo");
        this.loadData();
    }

    private void loadData(){
        if(codigo!=0){
            metodosWS.buscarIDWS(this, codigo, this, this);
        } else {
            Toast.makeText(this, "ERROR: No existe parámetros de búsqueda.", Toast.LENGTH_LONG).show();
        }
    }

    private void resultadoConsulta(JSONObject response){
        DiccionarioVO diccionarioVO = new DiccionarioVO();
        JSONArray jsonArray = response.optJSONArray("tbl_diccionario");
        try {
            JSONObject jsonObject = jsonArray.getJSONObject(0);
            //System.err.println(jsonObject);
            diccionarioVO.setId(jsonObject.getInt("id"));
            diccionarioVO.setPalabra(jsonObject.optString("palabra"));
            diccionarioVO.setSignificado(jsonObject.optString("significado"));
            txtPalabra.setText("" + diccionarioVO.getPalabra());
            txtSignificado.setText("" + diccionarioVO.getSignificado());
        } catch (Exception e){
            Toast.makeText(this, "Error referente a Buscar...", Toast.LENGTH_LONG).show();
            System.err.println("B------ " + e.getCause() + " ----- " + e.getMessage());
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        this.resultadoConsulta(response);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(this, "Error al response buscar datos. " + error, Toast.LENGTH_LONG).show();
        System.err.println("B------ " + error);
        txtPalabra.setText("");
        txtSignificado.setText("");
    }
}