package com.edutec.diccionario.complementos;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

public class MetodosWS {
    public static final String IPC_SERVER = "http://192.168.0.5:8080/ws/";
    Context context;
    RequestQueue requestQueue;
    JsonObjectRequest jsonObjectRequest;

    public MetodosWS() {
    }
    public void buscarIDWS(Context context, int parameto, Response.Listener listener, Response.ErrorListener errorListener){
        this.context = context;
        try{
            String url = IPC_SERVER + "diccionario_sw/buscar.php?codigo="+parameto;
            requestQueue = Volley.newRequestQueue(context);
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, listener, errorListener);
            requestQueue.add(jsonObjectRequest);
        } catch (Exception e){
            Toast.makeText(context, "ConflictoBuscar: " + e.getMessage(), Toast.LENGTH_LONG).show();
            System.err.println("Buscar: ----" + e.getCause() + " - " + e.getMessage());
        }
    }
    public void consultarWS(Context context, String buscar, Response.Listener listener, Response.ErrorListener errorListener){
        this.context = context;
        try {
            String url = IPC_SERVER + "diccionario_sw/mostrar.php?buscar="+buscar;
            System.err.println(url);
            requestQueue = Volley.newRequestQueue(context); // Algo nuevo
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, listener, errorListener);
            requestQueue.add(jsonObjectRequest);
        } catch (Exception e){
            Toast.makeText(context, "ConflictoMostrar: " + e.getMessage(), Toast.LENGTH_LONG).show();
            System.err.println("Mostrar: ----" + e.getCause() + " - " + e.getMessage());
        }
    }
}
