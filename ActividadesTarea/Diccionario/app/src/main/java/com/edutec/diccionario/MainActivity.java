package com.edutec.diccionario;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.edutec.diccionario.complementos.DiccionarioVO;
import com.edutec.diccionario.complementos.MetodosWS;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements Response.Listener<JSONObject>, Response.ErrorListener {
    ListView listView;
    ArrayList<String> listaDatos;
    ArrayList<DiccionarioVO> listaDiccionarioVO;
    EditText edtBuscar;
    MetodosWS metodosSW = new MetodosWS();
    String buscar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = findViewById(R.id.lvLista);
        edtBuscar = findViewById(R.id.edtBuscar);
        buscar = edtBuscar.getText().toString();
        metodosSW.consultarWS(this, buscar, this, this);
    }

    public void onClick(View view) {
        buscar = edtBuscar.getText().toString();
        metodosSW.consultarWS(this, buscar, this, this);
    }

    private void listarContenido(JSONObject response){
        JSONArray jsonArray = response.optJSONArray("tbl_diccionario");
        listaDiccionarioVO = new ArrayList<>();
        try {
            for(int i=0; i < jsonArray.length(); i++){
                DiccionarioVO diccionarioVO = new DiccionarioVO();
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                System.out.println(jsonObject);
                diccionarioVO.setId(jsonObject.getInt("id"));
                diccionarioVO.setPalabra(jsonObject.getString("palabra"));
                diccionarioVO.setSignificado(jsonObject.optString("significado"));
                listaDiccionarioVO.add(diccionarioVO);
            }
            listaDatos = new ArrayList<>();
            for(int i=0; i < listaDiccionarioVO.size(); i++){
                listaDatos.add(listaDiccionarioVO.get(i).getPalabra());
            }
            ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, listaDatos);
            listView.setAdapter(arrayAdapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    int codigo = listaDiccionarioVO.get(i).getId();
                    Intent intent = new Intent(getApplicationContext(), VistaActivityWS.class);
                    intent.putExtra("codigo", codigo);
                    startActivity(intent);
                }
            });
        } catch (Exception e){
            Toast.makeText(this, "Error referente a Mostrar...", Toast.LENGTH_LONG).show();
            System.err.println("M------ " + e.getCause() + " ----- " + e.getMessage());
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        this.listarContenido(response);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(this, "Error al response mostrar datos... " + error, Toast.LENGTH_LONG).show();
        System.err.println("L------ " + error);
    }
}