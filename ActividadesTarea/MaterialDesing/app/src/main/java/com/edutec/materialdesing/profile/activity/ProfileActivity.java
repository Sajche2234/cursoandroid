package com.edutec.materialdesing.profile.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.MenuItem;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.edutec.materialdesing.R;
import com.edutec.materialdesing.bottom.fragment.BottomFragment;
import com.edutec.materialdesing.floating.fragment.FloatingFragment;
import com.edutec.materialdesing.home.fragment.HomeFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class ProfileActivity extends AppCompatActivity {
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        connect();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true); // La amburguesa
        getSupportActionBar().setDisplayHomeAsUpEnabled(true); // Habilita la fecha de atras, a la par del titulo. Sin funcionamiento ni nada mas.
        getSupportActionBar().setTitle(R.string.nav_item_profile); // Imprime el titulo en el Barra.
        BottomNavigationView navigationView = findViewById(R.id.navigation);
        navigationView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener);
        displayView(new HomeFragment());
    }

    private BottomNavigationView.OnNavigationItemSelectedListener onNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            Fragment fragment;
            switch (menuItem.getItemId()){
                case R.id.navigation_email:
                    toolbar.setTitle("Email");
                    fragment = new HomeFragment();
                    return true;
                case R.id.navigation_plus:
                    toolbar.setTitle("Plus");
                    fragment = new FloatingFragment();
                    displayView(fragment);
                    return true;
                case R.id.navigation_shop:
                    toolbar.setTitle("Shop");
                    fragment = new BottomFragment();
                    displayView(fragment);
                    return true;
                case R.id.navigation_send:
                    toolbar.setTitle("Send");
                    fragment = new FloatingFragment();
                    displayView(fragment);
                    return true;
            }
            return false;
        }
    };

    private void displayView(Fragment fragment){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, fragment);
        fragmentTransaction.commit();
    }

    private void connect(){
        toolbar = findViewById(R.id.toolbar);
    }

    // Habilita las opciones para seleccionar el toolbar
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home: // Flechita para regresar. Nombre por defeco de la flecha.
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
