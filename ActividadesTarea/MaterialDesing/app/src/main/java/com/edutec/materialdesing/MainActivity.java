package com.edutec.materialdesing;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.edutec.materialdesing.bottom.fragment.BottomFragment;
import com.edutec.materialdesing.floating.fragment.FloatingFragment;
import com.edutec.materialdesing.home.fragment.HomeFragment;
import com.edutec.materialdesing.navigation.fragment.FragmentDrawer;
import com.edutec.materialdesing.profile.activity.ProfileActivity;

public class MainActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener {
    private Toolbar toolbar;
    private FragmentDrawer drawer;
    // https://material.io/resources/icons/?style=baseline
    // https://github.com/Clans/FloatingActionButton
    // https://material.io/design
    // https://material.io/components/cards#behavior

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        connect();
        setSupportActionBar(toolbar); // AndoirX
        //getSupportActionBar().setDisplayShowHomeEnabled(true);
        try {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        } catch (NullPointerException e){
            e.printStackTrace();
        }
        drawer=(FragmentDrawer)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawer.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);
        drawer.setDrawerListener(this);
        displayView(0);
    }

    private void displayView(int position) {
        Fragment fragment = null;
        String title = getString(R.string.app_name);

        switch (position) {
            case 0:
                fragment = new HomeFragment();
                title = getString(R.string.nav_item_home);
                break;

            case 1:
                fragment = new FloatingFragment();
                title = getString(R.string.nav_item_floating);
                break;

            case 2:
                fragment = new BottomFragment();
                title = getString(R.string.nav_item_bottom);
                break;

            case 3:
                startActivity(new Intent(this, ProfileActivity.class));
                break;

            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();

            getSupportActionBar().setTitle(title);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }
    private void connect(){
        toolbar=findViewById(R.id.toolbar);
    }
}
