package com.edutec.actividadetapa1;

public class PeliculasVO {
    // Componentes para el RecyclerView
    private int imagen;
    private int nombre;
    private int duracion;
    // Componentes para los detalles generales.
    private int sinopsis;
    private int reparto;
    private int puntuacion;
    private int precio;

    public PeliculasVO() {
    }

    public PeliculasVO(int imagen, int nombre, int duracion) {
        this.imagen = imagen;
        this.nombre = nombre;
        this.duracion = duracion;
    }

    public PeliculasVO(int sinopsis, int reparto, int puntuacion, int precio) {
        this.sinopsis = sinopsis;
        this.reparto = reparto;
        this.puntuacion = puntuacion;
        this.precio = precio;
    }

    public int getImagen() {
        return imagen;
    }

    public void setImagen(int imagen) {
        this.imagen = imagen;
    }

    public int getNombre() {
        return nombre;
    }

    public void setNombre(int nombre) {
        this.nombre = nombre;
    }

    public int getDuracion() {
        return duracion;
    }

    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }

    public int getSinopsis() {
        return sinopsis;
    }

    public void setSinopsis(int sinopsis) {
        this.sinopsis = sinopsis;
    }

    public int getReparto() {
        return reparto;
    }

    public void setReparto(int reparto) {
        this.reparto = reparto;
    }

    public int getPuntuacion() {
        return puntuacion;
    }

    public void setPuntuacion(int puntuacion) {
        this.puntuacion = puntuacion;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }
}
