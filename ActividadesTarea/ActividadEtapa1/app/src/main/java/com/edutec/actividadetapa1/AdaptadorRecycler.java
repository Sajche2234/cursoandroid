package com.edutec.actividadetapa1;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class AdaptadorRecycler extends RecyclerView.Adapter<AdaptadorRecycler.ViewHolder> {
    private ArrayList<PeliculasVO> list = new ArrayList<>();
    private static ClickListener clickListener;

    public AdaptadorRecycler(ArrayList<PeliculasVO> list) {
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item1, null, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.asignarDatos(list.get(position).getImagen(), list.get(position).getNombre(), list.get(position).getDuracion());
    }

    @Override
    public int getItemCount() {
        return this.list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imageView;
        TextView textView1, textView2;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            imageView = itemView.findViewById(R.id.imgRI1);
            textView1 = itemView.findViewById(R.id.txtNombreRI1);
            textView2 = itemView.findViewById(R.id.txtDuracionRI1);
        }
        public void asignarDatos(int img, int nombre, int duracion){
            imageView.setImageResource(img);
            textView1.setText(nombre);
            textView2.setText(duracion);
        }

        @Override
        public void onClick(View view) {
            clickListener.onItemClick(getAdapterPosition(), view);
        }
    }
    //
    public void setOnItemClickListener(ClickListener clickListener){
        AdaptadorRecycler.clickListener = clickListener;
    }
    //
    public interface ClickListener{
        public void onItemClick(int position, View v);
    }
}
