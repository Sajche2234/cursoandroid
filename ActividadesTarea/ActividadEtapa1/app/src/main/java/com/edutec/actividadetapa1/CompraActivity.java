package com.edutec.actividadetapa1;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class CompraActivity extends AppCompatActivity {
    TextView txtPrecio, txtTotal, txtNombre, txtApellido, txtNit;
    EditText txtCantidad;
    private TextWatcher texto = null;
    private int precio;
    private Double result;
    private int TIEMPO = 5000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compra);
        txtPrecio = findViewById(R.id.txtPrecio);
        txtCantidad = findViewById(R.id.txtCantidad);
        txtTotal = findViewById(R.id.txtTotal);
        txtNombre = findViewById(R.id.txtNombre);
        txtApellido = findViewById(R.id.txtApellido);
        txtNit = findViewById(R.id.txtNit);
        Bundle bundle =getIntent().getExtras();
        precio =bundle.getInt("precio");
        Double precioF =  Double.valueOf(precio);
        txtPrecio.setText("Q. " + precioF);
        txtCantidad.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                result = Double.valueOf(txtCantidad.getText().toString()) * precioF;
                txtTotal.setText("Q. "+result);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }
    public Double calculo(int precio, int cantidadP){
        Double resultado=0.0;
        //int cant = Integer.parseInt(txtCantidad.getText().toString());
        resultado = Double.valueOf(precio * cantidadP);
        return resultado;
    }

    public void confirmarCmpra(View view) {
        this.datos();
    }
    private void datos(){
        String nombre = txtNombre.getText().toString();
        String apellido = txtApellido.getText().toString();
        String nit = txtNit.getText().toString();
        String cantidad = txtCantidad.getText().toString();
        if((!nombre.isEmpty()) && (!apellido.isEmpty()) && (!nit.isEmpty()) && (!cantidad.isEmpty()) ){
            Toast.makeText(this, "Estimado " + nombre + ", " + apellido + ", con NIT: " + nit + ", su compra fue exitosa. Esperamos que disfrute de la película", Toast.LENGTH_LONG).show();
            this.finish();
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Debe de completar el formulario antes de confirmar la compra", Toast.LENGTH_LONG).show();
        }
    }
}