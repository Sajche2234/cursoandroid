package com.edutec.actividadetapa1;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private Toolbar toolbar1;
    private ArrayList<PeliculasVO> principales = new ArrayList<>();
    private ArrayList<PeliculasVO> especificos = new ArrayList<>();
    private RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar1 = findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar1);
        getSupportActionBar().setTitle("Cartelera");
        recyclerView = findViewById(R.id.recyclerID);
        this.viewLista();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_item1, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.item1:
                //Toast.makeText(this, "Lista", Toast.LENGTH_LONG).show();
                this.viewLista();
                break;
            case R.id.item2:
                //Toast.makeText(this, "Matriz", Toast.LENGTH_LONG).show();
                this.viewGrid();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    // Ingresar datos generales de la película.
    public void ingresarDatosPrincipales(){
        principales.add(new PeliculasVO(R.drawable.ic_movie1, R.string.name1, R.string.duracion1));
        principales.add(new PeliculasVO(R.drawable.ic_movie2, R.string.name2, R.string.duracion2));
        principales.add(new PeliculasVO(R.drawable.ic_movie3, R.string.name3, R.string.duracion3));
        principales.add(new PeliculasVO(R.drawable.ic_movie4, R.string.name4, R.string.duracion4));
    }
    // Ingresar datos adicionales de la pelicula.
    public void ingresarDatosEspecificos(){
        especificos.add(new PeliculasVO(R.string.sinopsis1, R.string.reparto1, R.string.puntaje1, 45));
        especificos.add(new PeliculasVO(R.string.sinopsis2, R.string.reparto2, R.string.puntaje2, 46));
        especificos.add(new PeliculasVO(R.string.sinopsis3, R.string.reparto3, R.string.puntaje3, 47));
        especificos.add(new PeliculasVO(R.string.sinopsis4, R.string.reparto4, R.string.puntaje4, 48));
    }
    // funcion para mostrar los datos en lista
    public void viewLista(){
        principales = new ArrayList<>();
        recyclerView.setLayoutManager(new GridLayoutManager(this, 1));
        this.ingresarDatosPrincipales();
        this.ingresarDatosEspecificos();
        AdaptadorRecycler adaptadorRecycler = new AdaptadorRecycler(principales);
        adaptadorRecycler.setOnItemClickListener(new AdaptadorRecycler.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                Intent intent;
                switch (position){
                    case 0:
                        intent = new Intent(getApplicationContext(), DetalleActivity.class);
                        intent.putExtra("nombre", principales.get(position).getNombre());
                        intent.putExtra("poster", principales.get(position).getImagen());
                        intent.putExtra("sinopsis", especificos.get(position).getSinopsis());
                        intent.putExtra("reparto", especificos.get(position).getReparto());
                        intent.putExtra("puntaje", especificos.get(position).getPuntuacion());
                        intent.putExtra("precio", especificos.get(position).getPrecio());
                        startActivity(intent);
                        break;
                    case 1:
                        intent = new Intent(getApplicationContext(), DetalleActivity.class);
                        intent.putExtra("nombre", principales.get(position).getNombre());
                        intent.putExtra("poster", principales.get(position).getImagen());
                        intent.putExtra("sinopsis", especificos.get(position).getSinopsis());
                        intent.putExtra("reparto", especificos.get(position).getReparto());
                        intent.putExtra("puntaje", especificos.get(position).getPuntuacion());
                        intent.putExtra("precio", especificos.get(position).getPrecio());
                        startActivity(intent);
                        break;
                    case 2:
                        intent = new Intent(getApplicationContext(), DetalleActivity.class);
                        intent.putExtra("nombre", principales.get(position).getNombre());
                        intent.putExtra("poster", principales.get(position).getImagen());
                        intent.putExtra("sinopsis", especificos.get(position).getSinopsis());
                        intent.putExtra("reparto", especificos.get(position).getReparto());
                        intent.putExtra("puntaje", especificos.get(position).getPuntuacion());
                        intent.putExtra("precio", especificos.get(position).getPrecio());
                        startActivity(intent);
                        break;
                    case 3:
                        intent = new Intent(getApplicationContext(), DetalleActivity.class);
                        intent.putExtra("nombre", principales.get(position).getNombre());
                        intent.putExtra("poster", principales.get(position).getImagen());
                        intent.putExtra("sinopsis", especificos.get(position).getSinopsis());
                        intent.putExtra("reparto", especificos.get(position).getReparto());
                        intent.putExtra("puntaje", especificos.get(position).getPuntuacion());
                        intent.putExtra("precio", especificos.get(position).getPrecio());
                        startActivity(intent);
                        break;
                }
            }
        });
        recyclerView.setAdapter(adaptadorRecycler);
    }
    // Función para mostrar los datos el matriz
    public void viewGrid(){
        principales = new ArrayList<>();
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        this.ingresarDatosPrincipales();
        AdaptadorRecycler adaptadorRecycler = new AdaptadorRecycler(principales);
        recyclerView.setAdapter(adaptadorRecycler);
    }
}