package com.edutec.actividadetapa1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class DetalleActivity extends AppCompatActivity {
    ImageView imgD;
    TextView txtNameD;
    int nombre;
    int poster;
    private Fragment fragment1, fragment2, fragment3;
    private FragmentTransaction transaction;
    int sinopsis, reparto, puntaje, precio;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle);
        // Inicializando componentes
        imgD = findViewById(R.id.imgD);
        txtNameD = findViewById(R.id.txtNameD);
        fragment1 = new Fragment1();
        fragment2 = new Fragment2();
        fragment3 = new Fragment3();
        // Obteniendo datos de la actividad anterior
        Bundle bundle = getIntent().getExtras();
        nombre = bundle.getInt("nombre");
        poster = bundle.getInt("poster");
        imgD.setImageResource(poster);
        txtNameD.setText(nombre);
        // Inicializando los fragmentos
        getSupportFragmentManager().beginTransaction().add(R.id.contenedorID, fragment1).commit();
        getSupportFragmentManager().beginTransaction().add(R.id.contenedorID, fragment2).hide(fragment2).commit();
        getSupportFragmentManager().beginTransaction().add(R.id.contenedorID, fragment3).hide(fragment3).commit();
        // Datos
        this.obtenerDatos();
        this.trasladarFrag1();
        this.trasladarFrag2();
        this.trasladarFrag3();
        //Toast.makeText(this, "Costo de Boelto: " + precio, Toast.LENGTH_LONG).show();
    }

    // Funciones para obtener los datos adicionales desde el bundle
    private void obtenerDatos(){
        Bundle bundle = getIntent().getExtras();
        sinopsis = bundle.getInt("sinopsis");
        reparto = bundle.getInt("reparto");
        puntaje = bundle.getInt("puntaje");
        precio = bundle.getInt("precio");
    }
    // Trasladar al Fragmento de Sinopsis
    private void trasladarFrag1(){
        Bundle bundle = new Bundle();
        bundle.putInt("sinopsis", sinopsis);
        fragment1.setArguments(bundle);
    }
    // Trasladar al Framento de Reparto
    private void trasladarFrag2(){
        Bundle bundle = new Bundle();
        bundle.putInt("reparto", reparto);
        fragment2.setArguments(bundle);
    }
    // Trasladar al Fragmento de Puntaje
    private void trasladarFrag3(){
        Bundle bundle = new Bundle();
        bundle.putInt("puntaje", puntaje);
        fragment3.setArguments(bundle);
    }

    public void onClick(MenuItem item) {
        transaction = getSupportFragmentManager().beginTransaction();
        switch (item.getItemId()){
            case R.id.item1D:
                if(fragment1.isAdded()){
                    transaction.hide(fragment3).hide(fragment2).show(fragment1);
                } else {
                    transaction.hide(fragment3).hide(fragment2).add(R.id.contenedorID, fragment1);
                    transaction.addToBackStack(null);
                }
                break;
            case R.id.item2D:
                if(fragment2.isAdded()){
                    transaction.hide(fragment3).hide(fragment1).show(fragment2);
                } else {
                    transaction.hide(fragment3).hide(fragment1).add(R.id.contenedorID, fragment2);
                    transaction.addToBackStack(null);
                }
                break;
            case R.id.item3D:
                if(fragment3.isAdded()) {
                    transaction.hide(fragment2).hide(fragment1).show(fragment3);
                } else {
                    transaction.hide(fragment2).hide(fragment1).add(R.id.contenedorID, fragment3);
                    transaction.addToBackStack(null);
                }
                break;
        }
        transaction.commit();
    }

    public void comprar(View view) {
        Intent intent = new Intent(this, CompraActivity.class);
        intent.putExtra("precio", precio);
        startActivity(intent);
    }

    public void mostrarImagen(View view) {
        Intent intImg = new Intent(this, ImagenActivity.class);
        intImg.putExtra("imagen", poster);
        startActivity(intImg);
        //Toast.makeText(this, "Hola mundo", Toast.LENGTH_SHORT).show();
    }
}