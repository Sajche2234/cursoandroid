package com.edutec.actividadetapa1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;

public class ImagenActivity extends AppCompatActivity {
    ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imagen);
        imageView = findViewById(R.id.imgLoad);
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            int imagen = bundle.getInt("imagen");
            imageView.setImageResource(imagen);
        }
    }
}