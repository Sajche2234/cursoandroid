package com.edutec.mywebservicelocal.complementos;

public class ProductoVO {
    private String cve_art, nombre_producto;
    private int stock_minimo, stock_maximo;
    private float precio_compra, precio_venta;
    private String estado;

    public ProductoVO() {
    }

    public ProductoVO(String cve_art, String nombre_producto, int stock_minimo, int stock_maximo, float precio_compra, float precio_venta, String estado) {
        this.cve_art = cve_art;
        this.nombre_producto = nombre_producto;
        this.stock_minimo = stock_minimo;
        this.stock_maximo = stock_maximo;
        this.precio_compra = precio_compra;
        this.precio_venta = precio_venta;
        this.estado = estado;
    }

    public String getCve_art() {
        return cve_art;
    }

    public void setCve_art(String cve_art) {
        this.cve_art = cve_art;
    }

    public String getNombre_producto() {
        return nombre_producto;
    }

    public void setNombre_producto(String nombre_producto) {
        this.nombre_producto = nombre_producto;
    }

    public int getStock_minimo() {
        return stock_minimo;
    }

    public void setStock_minimo(int stock_minimo) {
        this.stock_minimo = stock_minimo;
    }

    public int getStock_maximo() {
        return stock_maximo;
    }

    public void setStock_maximo(int stock_maximo) {
        this.stock_maximo = stock_maximo;
    }

    public float getPrecio_compra() {
        return precio_compra;
    }

    public void setPrecio_compra(float precio_compra) {
        this.precio_compra = precio_compra;
    }

    public float getPrecio_venta() {
        return precio_venta;
    }

    public void setPrecio_venta(float precio_venta) {
        this.precio_venta = precio_venta;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
