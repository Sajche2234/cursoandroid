package com.edutec.mywebservicelocal;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.edutec.mywebservicelocal.complementos.MetodosWS;
import com.edutec.mywebservicelocal.complementos.ProductoVO;

import org.json.JSONArray;
import org.json.JSONObject;

public class BuscarActivityWS extends AppCompatActivity implements Response.Listener<JSONObject>, Response.ErrorListener {
    EditText edtBuscar;
    TextView txtNombreP, txtSMin, txtSMax, txtPrecioC, txtPrecioV, txtEstadoP;
    MetodosWS metodosWS = new MetodosWS();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar_ws);
        edtBuscar = findViewById(R.id.edtBuscarProducto);
        txtNombreP = findViewById(R.id.txtNombreP);
        txtSMin = findViewById(R.id.txtSMin);
        txtSMax = findViewById(R.id.txtSMax);
        txtPrecioC = findViewById(R.id.txtPrecioC);
        txtPrecioV = findViewById(R.id.txtPrecioV);
        txtEstadoP = findViewById(R.id.txtEstadoP);
    }

    public void onClick(View view) {
        this.buscarParametro();
    }

    private void buscarParametro(){
        String buscar = edtBuscar.getText().toString();
        if(!buscar.isEmpty()) {
            metodosWS.buscarIDWS(this, buscar, this, this);
        } else {
            Toast.makeText(this, "Debe ingresar un parámetro de búsqueda", Toast.LENGTH_LONG).show();
        }
    }

    private void resultadoConsulta(JSONObject response){
        ProductoVO productoVO = new ProductoVO();
        JSONArray jsonArray = response.optJSONArray("tbl_producto");
        try {
            JSONObject jsonObject = jsonArray.getJSONObject(0);
            System.err.println(jsonObject);
            productoVO.setNombre_producto(jsonObject.optString("nombre_producto"));
            productoVO.setStock_minimo(jsonObject.optInt("stock_minimo"));
            productoVO.setStock_maximo(jsonObject.optInt("stock_maximo"));
            productoVO.setPrecio_compra(Float.parseFloat(jsonObject.optString("precio_compra")));
            productoVO.setPrecio_venta(Float.parseFloat(jsonObject.optString("precio_venta")));
            productoVO.setEstado(jsonObject.optString("estado"));
            System.err.println("Nombre: " + productoVO.getNombre_producto());
            System.err.println("Minimo: " + productoVO.getStock_minimo());
            System.err.println("Maximo: " + productoVO.getStock_maximo());
            System.err.println("Compra: " + productoVO.getPrecio_compra());
            System.err.println("Venta: " + productoVO.getPrecio_venta());
            System.err.println("Estado: " + productoVO.getEstado());
            txtNombreP.setText(productoVO.getNombre_producto());
            txtSMin.setText(String.valueOf(productoVO.getStock_minimo()));
            txtSMax.setText(String.valueOf(productoVO.getStock_maximo()));
            txtPrecioC.setText(String.valueOf(productoVO.getPrecio_compra()));
            txtPrecioV.setText(String.valueOf(productoVO.getPrecio_venta()));
            txtEstadoP.setText(String.valueOf(productoVO.getEstado()));
        } catch (Exception e){
            Toast.makeText(this, "Error referente a Buscar...", Toast.LENGTH_LONG).show();
            System.err.println("B------ " + e.getCause() + " ----- " + e.getMessage());
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        this.resultadoConsulta(response);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(this, "Error al response buscar datos. " + error, Toast.LENGTH_LONG).show();
        System.err.println("B------ " + error);
        txtNombreP.setText("");
        txtSMin.setText("");
        txtSMax.setText("");
        txtPrecioC.setText("");
        txtPrecioV.setText("");
        txtEstadoP.setText("");
    }
}