package com.edutec.mywebservicelocal.complementos;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.edutec.mywebservicelocal.R;

import org.json.JSONArray;
import org.json.JSONObject;

public class EliminarActivityWS extends AppCompatActivity implements Response.Listener<JSONObject>, Response.ErrorListener {
    EditText edtBuscar;
    TextView txtNombreP, txtSMin, txtSMax, txtPrecioC, txtPrecioV, txtEstadoP;
    MetodosWS metodosWS = new MetodosWS();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eliminar_ws);
        edtBuscar = findViewById(R.id.edtBuscarProducto);
        txtNombreP = findViewById(R.id.txtNombreP);
        txtSMin = findViewById(R.id.txtSMin);
        txtSMax = findViewById(R.id.txtSMax);
        txtPrecioC = findViewById(R.id.txtPrecioC);
        txtPrecioV = findViewById(R.id.txtPrecioV);
        txtEstadoP = findViewById(R.id.txtEstadoP);
    }

    public void onClick(View view) {
        switch (view.getId()){
            case R.id.idImgBuscar:
                this.buscarID();
                break;
            case R.id.btnEliminarP:
                this.eliminarP();
                break;
        }
    }

    private void buscarID(){
        String buscar = edtBuscar.getText().toString();
        if(!buscar.isEmpty()) {
            metodosWS.buscarIDWS(this, buscar, this, this);
        } else {
            Toast.makeText(this, "Debe ingresar un parámetro de búsqueda", Toast.LENGTH_LONG).show();
        }
    }

    private void eliminarP(){
        String buscar = edtBuscar.getText().toString();
        if(!buscar.isEmpty()){
            System.out.println(buscar);
            metodosWS.eliminarSW(this, Integer.parseInt(buscar), this, this);
            edtBuscar.setText("");
            txtNombreP.setText("");
            txtSMin.setText("");
            txtSMax.setText("");
            txtPrecioC.setText("");
            txtPrecioV.setText("");
            txtEstadoP.setText("");
            Toast.makeText(this, "Datos eliminados correctamente.", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Debe ingresar un codigo.", Toast.LENGTH_LONG).show();
        }
    }

    private void resultadoBusqueda(JSONObject response){
        ProductoVO productoVO = new ProductoVO();
        JSONArray jsonArray = response.optJSONArray("tbl_producto");
        try {
            JSONObject jsonObject = jsonArray.getJSONObject(0);
            System.err.println(jsonObject);
            productoVO.setNombre_producto(jsonObject.optString("nombre_producto"));
            productoVO.setStock_minimo(jsonObject.optInt("stock_minimo"));
            productoVO.setStock_maximo(jsonObject.optInt("stock_maximo"));
            productoVO.setPrecio_compra(Float.parseFloat(jsonObject.optString("precio_compra")));
            productoVO.setPrecio_venta(Float.parseFloat(jsonObject.optString("precio_venta")));
            productoVO.setEstado(jsonObject.optString("estado"));
            System.err.println("Nombre: " + productoVO.getNombre_producto());
            System.err.println("Minimo: " + productoVO.getStock_minimo());
            System.err.println("Maximo: " + productoVO.getStock_maximo());
            System.err.println("Compra: " + productoVO.getPrecio_compra());
            System.err.println("Venta: " + productoVO.getPrecio_venta());
            System.err.println("Estado: " + productoVO.getEstado());
            txtNombreP.setText(productoVO.getNombre_producto());
            txtSMin.setText(String.valueOf(productoVO.getStock_minimo()));
            txtSMax.setText(String.valueOf(productoVO.getStock_maximo()));
            txtPrecioC.setText(String.valueOf(productoVO.getPrecio_compra()));
            txtPrecioV.setText(String.valueOf(productoVO.getPrecio_venta()));
            txtEstadoP.setText(String.valueOf(productoVO.getEstado()));
        } catch (Exception e){
            Toast.makeText(this, "Error referente a Buscar...", Toast.LENGTH_LONG).show();
            System.err.println("B------ " + e.getCause() + " ----- " + e.getMessage());
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        this.resultadoBusqueda(response);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(this, "Error al response eliminar datos. " + error, Toast.LENGTH_LONG).show();
        System.err.println("E------ " + error);
        txtNombreP.setText("");
        txtSMin.setText("");
        txtSMax.setText("");
        txtPrecioC.setText("");
        txtPrecioV.setText("");
        txtEstadoP.setText("");
    }
}