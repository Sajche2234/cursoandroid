package com.edutec.mywebservicelocal.complementos;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

public class MetodosWS {
    public static final String IPC_SERVER = "http://192.168.0.36:8080/ws/";
    Context context;
    RequestQueue requestQueue;
    JsonObjectRequest jsonObjectRequest;

    public MetodosWS() {
    }
    public void insertarWS(Context context, String nombre, int minimo, int maximo, float compra, float venta, String estado, Response.Listener listener, Response.ErrorListener errorListener){
        this.context = context;
        try{
            String url = IPC_SERVER + "producto_sw/insertar.php?nombre_producto="+nombre+"&stock_minimo="+minimo+"&stock_maximo="+maximo+"&precio_compra="+compra+"&precio_venta="+venta+"&estado="+estado;
            requestQueue = Volley.newRequestQueue(context);
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, listener, errorListener);
            requestQueue.add(jsonObjectRequest);
        } catch (Exception e){
            Toast.makeText(context, "ConflictoInsertar: " + e.getMessage(), Toast.LENGTH_LONG).show();
            System.err.println("Insertar: ----" + e.getCause() + " - " + e.getMessage());
        }
    }
    public void buscarIDWS(Context context, String parameto, Response.Listener listener, Response.ErrorListener errorListener){
        this.context = context;
        try{
            String url = IPC_SERVER + "producto_sw/buscar.php?cve_art="+parameto;
            requestQueue = Volley.newRequestQueue(context);
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, listener, errorListener);
            requestQueue.add(jsonObjectRequest);
        } catch (Exception e){
            Toast.makeText(context, "ConflictoBuscar: " + e.getMessage(), Toast.LENGTH_LONG).show();
            System.err.println("Buscar: ----" + e.getCause() + " - " + e.getMessage());
        }
    }
    public void consultarWS(Context context, Response.Listener listener, Response.ErrorListener errorListener){
        this.context = context;
        try {
            String url = IPC_SERVER + "producto_sw/mostrar.php";
            requestQueue = Volley.newRequestQueue(context); // Algo nuevo
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, listener, errorListener);
            requestQueue.add(jsonObjectRequest);
        } catch (Exception e){
            Toast.makeText(context, "ConflictoMostrar: " + e.getMessage(), Toast.LENGTH_LONG).show();
            System.err.println("Mostrar: ----" + e.getCause() + " - " + e.getMessage());
        }
    }
    public void eliminarSW(Context context, int id, Response.Listener listener, Response.ErrorListener errorListener){
        this.context = context;
        try {
            String url = IPC_SERVER + "producto_sw/eliminar.php?cve_art="+id;
            requestQueue = Volley.newRequestQueue(context); // Algo nuevo
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, listener, errorListener);
            requestQueue.add(jsonObjectRequest);
        } catch (Exception e){
            Toast.makeText(context, "ConflictoEliminar: " + e.getMessage(), Toast.LENGTH_LONG).show();
            System.err.println("Eliminar: ----" + e.getCause() + " - " + e.getMessage());
        }
    }
}
