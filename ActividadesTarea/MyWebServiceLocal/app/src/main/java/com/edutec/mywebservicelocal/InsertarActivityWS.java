package com.edutec.mywebservicelocal;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.edutec.mywebservicelocal.complementos.MetodosWS;

import org.json.JSONObject;

public class InsertarActivityWS extends AppCompatActivity implements Response.Listener<JSONObject>, Response.ErrorListener {
    EditText edtNombre, edtSMin, edtSMax, edtCompra, edtVenta, edtEstado;
    MetodosWS metodosWS = new MetodosWS();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insertar_ws);
        edtNombre = findViewById(R.id.edtNombreP);
        edtSMin = findViewById(R.id.edtStockMin);
        edtSMax = findViewById(R.id.edtStockMax);
        edtCompra = findViewById(R.id.edtPrecioCompra);
        edtVenta = findViewById(R.id.edtPrecioVenta);
        edtEstado = findViewById(R.id.edtEstadoP);
    }

    public void onClick(View view) {
        this.insertar();
    }

    private void insertar(){
        String nombre, smin, smax, compra, venta, estado;
        nombre = edtNombre.getText().toString();
        smin = edtSMin.getText().toString();
        smax = edtSMax.getText().toString();
        compra = edtCompra.getText().toString();
        venta = edtVenta.getText().toString();
        estado = edtEstado.getText().toString();
        if(!nombre.isEmpty() && !smin.isEmpty() && !smax.isEmpty() && !compra.isEmpty() && !venta.isEmpty() && !estado.isEmpty()){
            metodosWS.insertarWS(this, nombre, Integer.parseInt(smin), Integer.parseInt(smax), Float.parseFloat(compra), Float.parseFloat(venta), estado, this, this);
            edtNombre.setText("");
            edtSMin.setText("");
            edtSMax.setText("");
            edtCompra.setText("");
            edtVenta.setText("");
            edtEstado.setText("");
        } else {
            Toast.makeText(this, "Debe llenar el formulario completo.", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        Toast.makeText(this, "Datos insertados correctamente", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(this, "Error al insertados datos. " + error, Toast.LENGTH_LONG).show();
        System.err.println("I------ " + error);
    }
}