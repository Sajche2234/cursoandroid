package com.edutec.mywebservicelocal;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.edutec.mywebservicelocal.complementos.MetodosWS;
import com.edutec.mywebservicelocal.complementos.ProductoVO;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class ListarActivityWS extends AppCompatActivity implements Response.Listener<JSONObject>, Response.ErrorListener {
    ListView listView;
    ArrayList<String> listaDatos;
    ArrayList<ProductoVO> listaProductoVO;
    MetodosWS metodosSW = new MetodosWS();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar_ws);
        listView = findViewById(R.id.lvListaProductos);
        metodosSW.consultarWS(this, this, this);
    }

    private void resultadoConsultaCompleta(JSONObject response){
        JSONArray jsonArray = response.optJSONArray("tbl_producto");
        listaProductoVO = new ArrayList<>();
        try {
            for(int i=0; i < jsonArray.length(); i++){
                ProductoVO productoVO = new ProductoVO();
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                System.out.println(jsonObject);
                productoVO.setCve_art(jsonObject.optString("cve_art"));
                productoVO.setNombre_producto(jsonObject.optString("nombre_producto"));
                productoVO.setStock_minimo(jsonObject.optInt("stock_minimo"));
                productoVO.setStock_maximo(jsonObject.optInt("stock_maximo"));
                productoVO.setPrecio_compra(Float.parseFloat(jsonObject.optString("precio_compra")));
                productoVO.setPrecio_venta(Float.parseFloat(jsonObject.optString("precio_venta")));
                productoVO.setEstado(jsonObject.optString("estado"));
                listaProductoVO.add(productoVO);
            }
            listaDatos = new ArrayList<>();
            for(int i=0; i < listaProductoVO.size(); i++){
                listaDatos.add(listaProductoVO.get(i).getCve_art() + ". " + listaProductoVO.get(i).getNombre_producto());
            }
            ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, listaDatos);
            listView.setAdapter(arrayAdapter);
        } catch (Exception e){
            Toast.makeText(this, "Error referente a Mostrar...", Toast.LENGTH_LONG).show();
            System.err.println("M------ " + e.getCause() + " ----- " + e.getMessage());
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        this.resultadoConsultaCompleta(response);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(this, "Error al response mostrar datos. " + error, Toast.LENGTH_LONG).show();
        System.err.println("B------ " + error);
    }
}