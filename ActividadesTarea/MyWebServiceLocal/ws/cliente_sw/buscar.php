<?php
require_once '../config/main.php';
$jsonArray = array();
$error=false;
$id_cliente	 = (isset($_REQUEST["id_cliente"]))?$_REQUEST["id_cliente"]:NULL;
if($id_cliente == NULL) {
	$resultadoVacio = array();
	$resultadoVacio["nombre_cliente"] = "";
	$resultadoVacio["apellido_cliente"] = "";
	$resultadoVacio["telefono_cliente"] = "";
	$resultadoVacio["direccion_cliente"] = "";
	$jsonArray['tbl_cliente'][] = $resultadoVacio;
	print json_encode($jsonArray);
	print "Datos no encontrados";
} else {
	$arrTMP = _assoc(_consulta("SELECT nombre_cliente, apellido_cliente, telefono_cliente, direccion_cliente FROM tbl_cliente WHERE id_cliente = $id_cliente"));
	if($arrTMP) {
		$jsonArray['tbl_cliente'][] = $arrTMP;
	} else {
		$resultadoVacio = array();
		$resultadoVacio["nombre_cliente"] = "";
		$resultadoVacio["apellido_cliente"] = "";
		$resultadoVacio["telefono_cliente"] = "";
		$resultadoVacio["direccion_cliente"] = "";
		$jsonArray['tbl_cliente'][] = $resultadoVacio;
	}
	print json_encode($jsonArray);
	_closeConectDB();
}
?>