package com.edutec.myappperritossqlite.complementos;

public class ConstantesSQL {
    public static final String BD_PERRO = "bd_perro";
    // Tabla
    public static final String TABLA_PERRO = "tbl_perro";
    // Campos
    public static final String CAMPO_ID = "id";
    public static final String CAMPO_NOMBRE = "nombre";
    public static final String CAMPO_RAZA = "raza";
    public static final String CAMPO_COLOR = "color";
    public static final String CAMPO_EDAD = "edad";
    // Crear, consulta.
    public static final String CREAR_TABLA_PERRO = "CREATE TABLE tbl_perro (id INTEGER PRIMARY KEY NOT NULL, nombre TEXT NOT NULL, raza TEXT NOT NULL, color TEXT NOT NULL, edad integer NOT NULL);";
    // DROP EN CASO DE QUE EXISTA
    public static final String BORRAR_TABLA = "DROP TABLE IF EXISTS tbl_perro";
    // VERSION
    public static final int VERSION =  1;
}
