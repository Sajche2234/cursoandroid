package com.edutec.myappperritossqlite;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class DetalleFragment extends Fragment {
    private String id, nombre, raza, color, edad;
    public DetalleFragment() {
        // Required empty public constructor
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            id = getArguments().getString("id");
            nombre = getArguments().getString("nombre");
            raza = getArguments().getString("raza");
            color = getArguments().getString("color");
            edad = getArguments().getString("edad");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_detalle, container, false);
        TextView txtId, txtNombre, txtRaza, txtColor, txtEdad;
        txtId = v.findViewById(R.id.idFrag);
        txtNombre = v.findViewById(R.id.nombreFrag);
        txtRaza = v.findViewById(R.id.razaFrag);
        txtColor = v.findViewById(R.id.colorFrag);
        txtEdad = v.findViewById(R.id.edadFrag);
        txtId.setText(id);
        txtNombre.setText(nombre);
        txtRaza.setText(raza);
        txtColor.setText(color);
        txtEdad.setText(edad);
        return v;
    }
}