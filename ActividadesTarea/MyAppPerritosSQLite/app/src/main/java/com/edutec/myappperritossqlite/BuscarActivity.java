package com.edutec.myappperritossqlite;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.edutec.myappperritossqlite.complementos.ConstantesSQL;

public class BuscarActivity extends AppCompatActivity {
    private EditText edtBuscarPerrito;
    private TextView txtNombre, txtRaza, txtColor, txtEdad;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar);
        edtBuscarPerrito = findViewById(R.id.edtBuscarPerrito);
        txtNombre = findViewById(R.id.txtNombre);
        txtRaza = findViewById(R.id.txtRaza);
        txtColor = findViewById(R.id.txtColor);
        txtEdad = findViewById(R.id.txtEdad);
    }

    public void onClick(View view) {
        this.buscarPerrito();
    }
    private void buscarPerrito(){
        ConectorSQLite conectorSQLite = new ConectorSQLite(this, ConstantesSQL.BD_PERRO, null, ConstantesSQL.VERSION);
        SQLiteDatabase database = conectorSQLite.getReadableDatabase();
        String[] parametro = { edtBuscarPerrito.getText().toString() };
        if(!edtBuscarPerrito.getText().toString().isEmpty()){
            try {
                String query="SELECT " + ConstantesSQL.CAMPO_NOMBRE + ", " + ConstantesSQL.CAMPO_RAZA + ", " + ConstantesSQL.CAMPO_COLOR + ", " + ConstantesSQL.CAMPO_EDAD + " FROM " +
                ConstantesSQL.TABLA_PERRO + " WHERE " + ConstantesSQL.CAMPO_ID + " = ?;";
                Cursor cursor = database.rawQuery(query, parametro);
                cursor.moveToFirst();
                txtNombre.setText(cursor.getString(0));
                txtRaza.setText(cursor.getString(1));
                txtColor.setText(cursor.getString(2));
                txtEdad.setText(cursor.getString(3) + " años.");
                cursor.close();
            } catch (Exception e){
                e.getMessage();
            }
        } else {
            Toast.makeText(this, "Debe ingresar un código existente en el campo de texto.", Toast.LENGTH_LONG).show();
        }
    }
}