package com.edutec.myappperritossqlite;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClick(View view) {
        Intent intent;
        switch (view.getId()){
            case R.id.btnInsertar:
                intent = new Intent(getApplicationContext(), InsertActivity.class);
                startActivity(intent);
                break;
            case R.id.btnBuscar:
                intent = new Intent(getApplicationContext(), BuscarActivity.class);
                startActivity(intent);
                break;
            case R.id.btnListar:
                intent = new Intent(getApplicationContext(), ListarActivity.class);
                startActivity(intent);
                break;
            case R.id.btnActualizar:
                intent = new Intent(getApplicationContext(), ActualizarActivity.class);
                startActivity(intent);
                break;
            case R.id.btnEliminar:
                intent = new Intent(getApplicationContext(), EliminarActivity.class);
                startActivity(intent);
                break;
        }
    }
}