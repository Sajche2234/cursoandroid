package com.edutec.myappperritossqlite;

import androidx.appcompat.app.AppCompatActivity;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.edutec.myappperritossqlite.complementos.ConstantesSQL;

public class InsertActivity extends AppCompatActivity {
    EditText edtNombre, edtRaza, edtColor, edtEdad;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert);
        edtNombre = findViewById(R.id.edtNombre);
        edtRaza = findViewById(R.id.edtRaza);
        edtColor = findViewById(R.id.edtColor);
        edtEdad = findViewById(R.id.edtEdad);
    }

    public void onClick(View view) {
        this.insertPerrito();
    }
    private void insertPerrito(){
        String nombre = edtNombre.getText().toString();
        String raza = edtRaza.getText().toString();
        String color = edtColor.getText().toString();
        String edad = edtEdad.getText().toString();
        if(!nombre.isEmpty() && !raza.isEmpty() && !color.isEmpty() && !edad.isEmpty()){
            ConectorSQLite conectorSQLite = new ConectorSQLite(this, ConstantesSQL.BD_PERRO, null, ConstantesSQL.VERSION);
            SQLiteDatabase database = conectorSQLite.getWritableDatabase();
            try {
                String query = "INSERT INTO " + ConstantesSQL.TABLA_PERRO + "("+ConstantesSQL.CAMPO_NOMBRE+", "+ConstantesSQL.CAMPO_RAZA+", "+ConstantesSQL.CAMPO_COLOR+", "+ConstantesSQL.CAMPO_EDAD+") " +
                        "VALUES ('"+nombre+"', '"+raza+"', '"+color+"', "+edad+");";
                database.execSQL(query);
                database.close();
                edtNombre.setText("");
                edtRaza.setText("");
                edtColor.setText("");
                edtEdad.setText("");
            } catch (Exception e){
                e.getMessage();
            }
        } else {
            Toast.makeText(this, "Debe de llenar todos los campos", Toast.LENGTH_LONG).show();
        }
    }
}