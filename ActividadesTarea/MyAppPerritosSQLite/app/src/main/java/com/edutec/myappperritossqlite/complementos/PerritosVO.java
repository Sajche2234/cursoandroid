package com.edutec.myappperritossqlite.complementos;

public class PerritosVO {
    private int perritoId;
    private String perritoNombre;
    private String perritoRaza;
    private String perritoColor;
    private int perritoEdad;

    public PerritosVO() {
    }

    public PerritosVO(int perritoId, String perritoNombre, String perritoRaza, String perritoColor, int perritoEdad) {
        this.perritoId = perritoId;
        this.perritoNombre = perritoNombre;
        this.perritoRaza = perritoRaza;
        this.perritoColor = perritoColor;
        this.perritoEdad = perritoEdad;
    }

    public int getPerritoId() {
        return perritoId;
    }

    public void setPerritoId(int perritoId) {
        this.perritoId = perritoId;
    }

    public String getPerritoNombre() {
        return perritoNombre;
    }

    public void setPerritoNombre(String perritoNombre) {
        this.perritoNombre = perritoNombre;
    }

    public String getPerritoRaza() {
        return perritoRaza;
    }

    public void setPerritoRaza(String perritoRaza) {
        this.perritoRaza = perritoRaza;
    }

    public String getPerritoColor() {
        return perritoColor;
    }

    public void setPerritoColor(String perritoColor) {
        this.perritoColor = perritoColor;
    }

    public int getPerritoEdad() {
        return perritoEdad;
    }

    public void setPerritoEdad(int perritoEdad) {
        this.perritoEdad = perritoEdad;
    }
}
