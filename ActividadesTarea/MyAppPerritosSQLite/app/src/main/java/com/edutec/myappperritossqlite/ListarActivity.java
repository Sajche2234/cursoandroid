package com.edutec.myappperritossqlite;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.edutec.myappperritossqlite.complementos.ConstantesSQL;
import com.edutec.myappperritossqlite.complementos.PerritosVO;

import java.util.ArrayList;

public class ListarActivity extends AppCompatActivity {
    private ListView listView;
    ArrayList<String> listaDatos;
    ArrayList<PerritosVO> listaPerritos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar);
        listView = findViewById(R.id.listaMostrar);
        this.mostrarPerritos();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                trasladarInfo(i);
            }
        });
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, listaDatos);
        listView.setAdapter(arrayAdapter);
    }
    private void mostrarPerritos(){
        ConectorSQLite conectorSQLite = new ConectorSQLite(this, ConstantesSQL.BD_PERRO, null, ConstantesSQL.VERSION);
        SQLiteDatabase database = conectorSQLite.getReadableDatabase();
        try {
            PerritosVO perritosVO;
            listaPerritos = new ArrayList<>();
            String query = "SELECT * FROM " + ConstantesSQL.TABLA_PERRO + ";";
            Cursor cursor = database.rawQuery(query, null);
            while(cursor.moveToNext()){
                perritosVO = new PerritosVO();
                perritosVO.setPerritoId(cursor.getInt(0));
                perritosVO.setPerritoNombre(cursor.getString(1));
                perritosVO.setPerritoRaza(cursor.getString(2));
                perritosVO.setPerritoColor(cursor.getString(3));
                perritosVO.setPerritoEdad(cursor.getInt(4));
                listaPerritos.add(perritosVO);
            }
            listaDatos = new ArrayList<>();
            for(int i=0; i < listaPerritos.size(); i++){
                listaDatos.add(listaPerritos.get(i).getPerritoId() + ". " + listaPerritos.get(i).getPerritoNombre());
            }
        } catch (Exception e){
            e.getMessage();
        }
    }
    private void trasladarInfo(int posicion){
        String idP, nombreP, razaP, colorP, edadP;
        idP = String.valueOf(listaPerritos.get(posicion).getPerritoId());
        nombreP = listaPerritos.get(posicion).getPerritoNombre();
        razaP = listaPerritos.get(posicion).getPerritoRaza();
        colorP = listaPerritos.get(posicion).getPerritoColor();
        edadP = String.valueOf(listaPerritos.get(posicion).getPerritoEdad());
        Intent intent = new Intent(getApplicationContext(), DetalleActivity.class);
        intent.putExtra("id", idP);
        intent.putExtra("nombre", nombreP);
        intent.putExtra("raza", razaP);
        intent.putExtra("color", colorP);
        intent.putExtra("edad", edadP);
        startActivity(intent);
    }
}