package com.edutec.myappperritossqlite;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.edutec.myappperritossqlite.complementos.ConstantesSQL;

public class ActualizarActivity extends AppCompatActivity {
    private EditText edtBuscar, edtNombre, edtRaza, edtColor, edtEdad;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actualizar);
        edtBuscar = findViewById(R.id.edtBuscarPerritoAc);
        edtNombre = findViewById(R.id.edtNombreAc);
        edtRaza = findViewById(R.id.edtRazaAc);
        edtColor = findViewById(R.id.edtColorAc);
        edtEdad = findViewById(R.id.edtEdadAc);
    }

    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnBuscarPerritoAc:
                this.consultarID();
                break;
            case R.id.btnActualizarPerritoAc:
                this.actualizarPerrito();
                break;
        }
    }
    private void consultarID(){
        if(!edtBuscar.getText().toString().isEmpty()){
            ConectorSQLite conectorSQLite = new ConectorSQLite(this, ConstantesSQL.BD_PERRO, null, ConstantesSQL.VERSION);
            SQLiteDatabase database = conectorSQLite.getReadableDatabase();
            String[] parametro = { edtBuscar.getText().toString() };
            try {
                String query = "SELECT * FROM " + ConstantesSQL.TABLA_PERRO + " WHERE " + ConstantesSQL.CAMPO_ID + " =?;";
                Cursor cursor = database.rawQuery(query, parametro);
                cursor.moveToFirst();
                edtNombre.setText(cursor.getString(1));
                edtRaza.setText(cursor.getString(2));
                edtColor.setText(cursor.getString(3));
                edtEdad.setText(cursor.getString(4));
                cursor.close();
            } catch (Exception e) {
                e.getMessage();
            }
        } else {
            Toast.makeText(this, "Debe ingresar el código existente.", Toast.LENGTH_LONG).show();
        }
    }
    private void actualizarPerrito(){
        String nombre, raza, color, edad, id;
        id = edtBuscar.getText().toString();
        nombre = edtNombre.getText().toString();
        raza = edtRaza.getText().toString();
        color = edtColor.getText().toString();
        edad = edtEdad.getText().toString();
        if(!nombre.isEmpty() && !raza.isEmpty() && !color.isEmpty() && !edad.isEmpty()){
            ConectorSQLite conectorSQLite = new ConectorSQLite(this, ConstantesSQL.BD_PERRO, null, ConstantesSQL.VERSION);
            SQLiteDatabase database = conectorSQLite.getWritableDatabase();
            String query = "UPDATE " + ConstantesSQL.TABLA_PERRO + " SET " +
                    ConstantesSQL.CAMPO_NOMBRE + " = '" + nombre + "', " +
                    ConstantesSQL.CAMPO_RAZA + " = '" + raza + "', " +
                    ConstantesSQL.CAMPO_COLOR + " = '" + color + "', " +
                    ConstantesSQL.CAMPO_EDAD + " = " + edad + " WHERE " +
                    ConstantesSQL.CAMPO_ID + " = " + id + ";";
            database.execSQL(query);
            database.close();
            Toast.makeText(this, "Datos actualizados correctamente", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Debe completar el formulario.", Toast.LENGTH_LONG).show();
        }
    }
}