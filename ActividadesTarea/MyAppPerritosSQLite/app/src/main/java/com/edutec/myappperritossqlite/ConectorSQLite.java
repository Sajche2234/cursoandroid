package com.edutec.myappperritossqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import com.edutec.myappperritossqlite.complementos.ConstantesSQL;

public class ConectorSQLite extends SQLiteOpenHelper {
    public ConectorSQLite(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(ConstantesSQL.CREAR_TABLA_PERRO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(ConstantesSQL.BORRAR_TABLA);
        onCreate(sqLiteDatabase);
    }
}
