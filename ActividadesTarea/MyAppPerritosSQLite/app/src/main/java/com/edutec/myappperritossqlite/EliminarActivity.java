package com.edutec.myappperritossqlite;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.edutec.myappperritossqlite.complementos.ConstantesSQL;

public class EliminarActivity extends AppCompatActivity {
    private EditText edtBuscar;
    private TextView txtNombre, txtRaza, txtColor, txtEdad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eliminar);
        edtBuscar = findViewById(R.id.edtBuscarPerritoEl);
        txtNombre = findViewById(R.id.txtNombreEl);
        txtRaza = findViewById(R.id.txtRazaEl);
        txtColor = findViewById(R.id.txtColorEl);
        txtEdad = findViewById(R.id.txtEdadEl);
    }

    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnBuscarPerritoEl:
                this.consultarID();
                break;
            case R.id.btnEliminarPerritoEl:
                this.eliminarPerrito();
                break;
        }
    }
    private void consultarID(){
        if(!edtBuscar.getText().toString().isEmpty()){
            ConectorSQLite conectorSQLite = new ConectorSQLite(this, ConstantesSQL.BD_PERRO, null, ConstantesSQL.VERSION);
            SQLiteDatabase database = conectorSQLite.getReadableDatabase();
            String[] parametro = { edtBuscar.getText().toString() };
            try {
                String query = "SELECT * FROM " + ConstantesSQL.TABLA_PERRO + " WHERE " + ConstantesSQL.CAMPO_ID + " =?;";
                Cursor cursor = database.rawQuery(query, parametro);
                cursor.moveToFirst();
                txtNombre.setText(cursor.getString(1));
                txtRaza.setText(cursor.getString(2));
                txtColor.setText(cursor.getString(3));
                txtEdad.setText(cursor.getString(4));
                cursor.close();
            } catch (Exception e) {
                e.getMessage();
            }
        } else {
            Toast.makeText(this, "Debe ingresar el código existente.", Toast.LENGTH_LONG).show();
        }
    }
    private void eliminarPerrito(){
        String id;
        id = edtBuscar.getText().toString();
        if(!id.isEmpty()){
            ConectorSQLite conectorSQLite = new ConectorSQLite(this, ConstantesSQL.BD_PERRO, null, ConstantesSQL.VERSION);
            SQLiteDatabase database = conectorSQLite.getWritableDatabase();
            String query = "DELETE FROM " + ConstantesSQL.TABLA_PERRO + " WHERE " + ConstantesSQL.CAMPO_ID + " = " + id + ";";
            database.execSQL(query);
            database.close();
            Toast.makeText(this, "Datos Eliminados correctamente", Toast.LENGTH_LONG).show();
            this.finish();
        } else {
            Toast.makeText(this, "Debe completar el formulario.", Toast.LENGTH_LONG).show();
        }
    }
}