package com.edutec.recyclerviewclasedatos;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private ArrayList<Contacto> listaContacto = new ArrayList<>();
    private RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recyclerId);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        this.insertarDatos();
        AdaptadorRecycler adaptadorRecycler = new AdaptadorRecycler(listaContacto);
        recyclerView.setAdapter(adaptadorRecycler);
    }
    public void insertarDatos(){
        listaContacto.add(new Contacto("Maco", "11111111", "Claro", R.drawable.ic_img_1));
        listaContacto.add(new Contacto("Maria", "22222222", "Tigo", R.drawable.ic_img_2));
        listaContacto.add(new Contacto("Pedro", "33333333", "Movistar", R.drawable.ic_img_3));
        listaContacto.add(new Contacto("Elizabeth", "44444444", "Claro", R.drawable.ic_img_4));
        listaContacto.add(new Contacto("Olga", "55555555", "Tigo", R.drawable.ic_img_5));
        listaContacto.add(new Contacto("Ana", "66666666", "Movistar", R.drawable.ic_img_6));
        listaContacto.add(new Contacto("Rosa", "77777777", "Claro", R.drawable.ic_img_7));
        listaContacto.add(new Contacto("Roberto", "88888888", "Tigo", R.drawable.ic_img_8));
        listaContacto.add(new Contacto("Andrea", "999999999", "Movistar", R.drawable.ic_img_9));
        listaContacto.add(new Contacto("Mayda", "10101010", "Claro", R.drawable.ic_img_10));
        listaContacto.add(new Contacto("Francisco", "111111", "Claro", R.drawable.ic_img_11));
        listaContacto.add(new Contacto("Eulalia", "12121212", "Claro", R.drawable.ic_img_12));
    }
}