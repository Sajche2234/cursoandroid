package com.edutec.recyclerviewclasedatos;

public class Contacto {
    private String nombreR, telefonoR, companiaR;
    private int imagenR;

    public Contacto() {
    }

    public Contacto(String nombreR, String telefonoR, String companiaR, int imagenR) {
        this.nombreR = nombreR;
        this.telefonoR = telefonoR;
        this.companiaR = companiaR;
        this.imagenR = imagenR;
    }

    public String getNombreR() {
        return nombreR;
    }

    public void setNombreR(String nombreR) {
        this.nombreR = nombreR;
    }

    public String getTelefonoR() {
        return telefonoR;
    }

    public void setTelefonoR(String telefonoR) {
        this.telefonoR = telefonoR;
    }

    public String getCompaniaR() {
        return companiaR;
    }

    public void setCompaniaR(String companiaR) {
        this.companiaR = companiaR;
    }

    public int getImagenR() {
        return imagenR;
    }

    public void setImagenR(int imagenR) {
        this.imagenR = imagenR;
    }
}
