package com.edutec.recyclerviewclasedatos;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class AdaptadorRecycler extends RecyclerView.Adapter<AdaptadorRecycler.ViewHolder> {
    private ArrayList<Contacto> listaContactos = new ArrayList<>();

    public AdaptadorRecycler(ArrayList<Contacto> listaContactos) {
        this.listaContactos = listaContactos;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item, null, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull AdaptadorRecycler.ViewHolder holder, int position) {
        holder.asignarDatos(
                listaContactos.get(position).getNombreR(),
                listaContactos.get(position).getTelefonoR(),
                listaContactos.get(position).getCompaniaR(),
                listaContactos.get(position).getImagenR()
        );
    }

    @Override
    public int getItemCount() {
        return this.listaContactos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewNombre, textViewNumero, textViewCompania;
        ImageView imageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewNombre = itemView.findViewById(R.id.txtNombreR);
            textViewNumero = itemView.findViewById(R.id.txtTelefonoR);
            textViewCompania = itemView.findViewById(R.id.txtCompania);
            imageView = itemView.findViewById(R.id.imgR);
        }
        public void asignarDatos(String nombre, String numero, String compania, int imagen){
            textViewNombre.setText(nombre);
            textViewNumero.setText(numero);
            textViewCompania.setText(compania);
            imageView.setImageResource(imagen);
        }
    }
}
