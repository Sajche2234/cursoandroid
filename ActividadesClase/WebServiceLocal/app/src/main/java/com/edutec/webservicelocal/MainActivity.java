package com.edutec.webservicelocal;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClick(View view) {
        Intent intent;
        switch(view.getId()) {
            case R.id.btnInsertar:
                intent = new Intent(getApplicationContext(), InsertarActivitySW.class);
                startActivity(intent);
                break;
            case R.id.btnBuscar:
                intent = new Intent(getApplicationContext(), BuscarActivitySW.class);
                startActivity(intent);
                break;
            case R.id.btnListar:
                intent = new Intent(getApplicationContext(), ListarActivityWS.class);
                startActivity(intent);
                break;
            case R.id.btnDelete:
                intent = new Intent(getApplicationContext(), EliminarActivitySW.class);
                startActivity(intent);
                break;
            case R.id.btnEdit:
                intent = new Intent(getApplicationContext(), ActualizarActivitySW.class);
                startActivity(intent);
                break;
        }
    }
}