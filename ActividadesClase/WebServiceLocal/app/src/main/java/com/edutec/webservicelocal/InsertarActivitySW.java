package com.edutec.webservicelocal;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.edutec.webservicelocal.complementos.MetodosSW;

import org.json.JSONObject;

public class InsertarActivitySW extends AppCompatActivity implements Response.Listener<JSONObject>, Response.ErrorListener {
    EditText nombre, apellido, telefono, direccion;
    MetodosSW metodosSW = new MetodosSW();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insertar_sw);
        nombre = findViewById(R.id.edtNombreCliente);
        apellido = findViewById(R.id.edtApellidoCliente);
        telefono = findViewById(R.id.edtTelefonoCliente);
        direccion = findViewById(R.id.edtDireccionCliente);
    }

    public void onClick(View view) {
        this.insertar();
    }

    private void insertar(){
        String strNombre, strApellido, strTelefono, strDireccion;
        strNombre = nombre.getText().toString();
        strApellido = apellido.getText().toString();
        strTelefono = telefono.getText().toString();
        strDireccion = direccion.getText().toString();
        if(!strNombre.isEmpty() && !strApellido.isEmpty() && !strTelefono.isEmpty() && !strDireccion.isEmpty()){
            // Utilizamos el metodo respectivo.
            metodosSW.insertarSW(this, strNombre, strApellido, Integer.parseInt(strTelefono), strDireccion, this, this);
            nombre.setText("");
            apellido.setText("");
            telefono.setText("");
            direccion.setText("");
        } else {
            Toast.makeText(this, "Debe llenar el formulario completo.", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        Toast.makeText(this, "Datos insertados correctamente", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(this, "Error al insertados datos. " + error, Toast.LENGTH_LONG).show();
        System.err.println("I------ " + error);
    }
}