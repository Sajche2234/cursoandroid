package com.edutec.webservicelocal;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.edutec.webservicelocal.complementos.CleinteVO;
import com.edutec.webservicelocal.complementos.MetodosSW;

import org.json.JSONArray;
import org.json.JSONObject;

public class EliminarActivitySW extends AppCompatActivity implements Response.Listener<JSONObject>, Response.ErrorListener {
    EditText edtEliminarCliente;
    TextView txtNombreC, txtApellidoC, txtTelefonoC, txtDireccionC;
    MetodosSW metodosSW = new MetodosSW();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eliminar_sw);
        edtEliminarCliente = findViewById(R.id.edtEliminarCliente);
        txtNombreC = findViewById(R.id.txtNombreC);
        txtApellidoC = findViewById(R.id.txtApellidoC);
        txtTelefonoC = findViewById(R.id.txtTelefonoC);
        txtDireccionC = findViewById(R.id.txtDireccionC);
    }

    public void onClick(View view) {
        switch (view.getId()){
            case R.id.idImgEliminar:
                this.buscarID();
                break;
            case R.id.btnEliminarC:
                System.out.println("Hola mundo");
                this.eliminarC();
                break;
        }
    }

    private void eliminarC(){
        String buscar = edtEliminarCliente.getText().toString();
        System.out.println(buscar + "........");
        if(!buscar.isEmpty()){
            System.out.println(buscar);
            metodosSW.eliminarSW(this, Integer.parseInt(buscar), this, this);
            edtEliminarCliente.setText("");
            txtNombreC.setText("");
            txtApellidoC.setText("");
            txtTelefonoC.setText("");
            txtDireccionC.setText("");
            Toast.makeText(this, "Debe eliminados correctamente.", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Debe ingresar un codigo.", Toast.LENGTH_LONG).show();
        }
    }

    private void buscarID(){
        String buscar = edtEliminarCliente.getText().toString();
        if(!buscar.isEmpty()){
            metodosSW.buscarDIWS(this, Integer.parseInt(buscar), this, this);
        } else {
            Toast.makeText(this, "Debe de llenar el campo", Toast.LENGTH_LONG).show();
        }
    }

    private void resultadoBusqueda(JSONObject response){
        CleinteVO clienteVO = new CleinteVO();
        JSONArray jsonArray = response.optJSONArray("tbl_cliente");
        try {
            JSONObject jsonObject = jsonArray.getJSONObject(0);
            System.out.println(jsonObject);
            clienteVO.setId_cliente(jsonObject.optInt("id_cliente"));
            clienteVO.setNombre_cliente(jsonObject.optString("nombre_cliente"));
            clienteVO.setApellido_cliente(jsonObject.optString("apellido_cliente"));
            clienteVO.setTelefono_cliente(jsonObject.optInt("telefono_cliente"));
            clienteVO.setDireccion_cliente(jsonObject.optString("direccion_cliente"));

            txtNombreC.setText(clienteVO.getNombre_cliente());
            txtApellidoC.setText(clienteVO.getApellido_cliente());
            txtTelefonoC.setText(String.valueOf(clienteVO.getTelefono_cliente()));
            txtDireccionC.setText(clienteVO.getDireccion_cliente());
        } catch (Exception e){
            Toast.makeText(this, "Error referente a Eliminar...", Toast.LENGTH_LONG).show();
            System.err.println("E------ " + e.getCause() + " ----- " + e.getMessage());
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        this.resultadoBusqueda(response);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(this, "Error al response Eliminar datos. " + error, Toast.LENGTH_LONG).show();
        System.err.println("B------ " + error);
        txtNombreC.setText("");
        txtApellidoC.setText("");
        txtTelefonoC.setText("");
        txtDireccionC.setText("");
    }
}