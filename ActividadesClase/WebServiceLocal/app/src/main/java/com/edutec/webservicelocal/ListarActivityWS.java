package com.edutec.webservicelocal;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.edutec.webservicelocal.complementos.CleinteVO;
import com.edutec.webservicelocal.complementos.MetodosSW;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class ListarActivityWS extends AppCompatActivity implements Response.Listener<JSONObject>, Response.ErrorListener {
    ListView listView;
    ArrayList<String> listaDatos;
    ArrayList<CleinteVO> listaClienteVO;
    MetodosSW metodosSW = new MetodosSW();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar_ws);
        listView = findViewById(R.id.lvListaClientes);
        metodosSW.consultarSW(this, this, this);
    }

    private void resultadoConsultaCompleta(JSONObject response){
        //CleinteVO clienteVO = new CleinteVO();
        JSONArray jsonArray = response.optJSONArray("tbl_cliente");
        listaClienteVO = new ArrayList<>();
        try {
            for(int i=0; i < jsonArray.length(); i++){
                CleinteVO clienteVO = new CleinteVO();
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                System.out.println(jsonObject);
                clienteVO.setId_cliente(jsonObject.optInt("id_cliente"));
                clienteVO.setNombre_cliente(jsonObject.optString("nombre_cliente"));
                clienteVO.setApellido_cliente(jsonObject.optString("apellido_cliente"));
                clienteVO.setTelefono_cliente(jsonObject.optInt("telefono_cliente"));
                clienteVO.setDireccion_cliente(jsonObject.optString("direccion_cliente"));
                listaClienteVO.add(clienteVO);
            }
            listaDatos = new ArrayList<>();
            for(int i=0; i < listaClienteVO.size(); i++){
                listaDatos.add(listaClienteVO.get(i).getId_cliente() + ". " + listaClienteVO.get(i).getNombre_cliente());
            }
            ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, listaDatos);
            listView.setAdapter(arrayAdapter);
        } catch (Exception e){
            Toast.makeText(this, "Error referente a Mostrar...", Toast.LENGTH_LONG).show();
            System.err.println("M------ " + e.getCause() + " ----- " + e.getMessage());
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        this.resultadoConsultaCompleta(response);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(this, "Error al response mostrar datos. " + error, Toast.LENGTH_LONG).show();
        System.err.println("B------ " + error);
    }
}