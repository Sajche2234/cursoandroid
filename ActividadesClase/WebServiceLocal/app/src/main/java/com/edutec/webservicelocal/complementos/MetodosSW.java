package com.edutec.webservicelocal.complementos;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

public class MetodosSW {
    // Constante con la IP del servidir de red local
    public static final String IPC_SERVER = "http://10.0.3.57:8080/ws/";
    // Implementar variables para la conexión y obtención de información
    Context context;
    RequestQueue requestQueue;
    JsonObjectRequest jsonObjectRequest;
    public MetodosSW() {
    }
    // Metodo de insertar
    public void insertarSW(Context context, String nombre, String apellido, int telefono, String direccion, Response.Listener listener, Response.ErrorListener errorListener){
        this.context = context;
        try{
            String url = IPC_SERVER + "cliente_sw/insertar.php?nombre_cliente="+nombre+"&apellido_cliente="+apellido+"&telefono_cliente="+telefono+"&direccion_cliente="+direccion;
            requestQueue = Volley.newRequestQueue(context);
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, listener, errorListener);
            requestQueue.add(jsonObjectRequest);
        } catch ( Exception e){
            Toast.makeText(context, "ConflictoInsertar: " + e.getMessage(), Toast.LENGTH_LONG).show();
            System.err.println("Insertar: ----" + e.getCause() + " - " + e.getMessage());
        }
    }
    // Método buscar
    public void buscarDIWS(Context context, int id, Response.Listener listener, Response.ErrorListener errorListener){
        this.context = context;
        try {
            String url = IPC_SERVER + "cliente_sw/buscar.php?id_cliente="+id;
            requestQueue = Volley.newRequestQueue(context); // Algo nuevo
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, listener, errorListener);
            requestQueue.add(jsonObjectRequest);
        } catch (Exception e ){
            Toast.makeText(context, "ConflictoBuscar: " + e.getMessage(), Toast.LENGTH_LONG).show();
            System.err.println("Buscar: ----" + e.getCause() + " - " + e.getMessage());
        }
    }
    // Método Mostrar
    public void consultarSW(Context context, Response.Listener listener, Response.ErrorListener errorListener){
        this.context = context;
        try {
            String url = IPC_SERVER + "cliente_sw/mostrar.php";
            requestQueue = Volley.newRequestQueue(context); // Algo nuevo
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, listener, errorListener);
            requestQueue.add(jsonObjectRequest);
        } catch (Exception e){
            Toast.makeText(context, "ConflictoMostrar: " + e.getMessage(), Toast.LENGTH_LONG).show();
            System.err.println("Mostrar: ----" + e.getCause() + " - " + e.getMessage());
        }
    }
    // Método Eliminar
    public void eliminarSW(Context context, int id, Response.Listener listener, Response.ErrorListener errorListener){
        this.context = context;
        try {
            String url = IPC_SERVER + "cliente_sw/eliminar.php?id_cliente="+id;
            requestQueue = Volley.newRequestQueue(context); // Algo nuevo
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, listener, errorListener);
            requestQueue.add(jsonObjectRequest);
        } catch (Exception e){
            Toast.makeText(context, "ConflictoEliminar: " + e.getMessage(), Toast.LENGTH_LONG).show();
            System.err.println("Eliminar: ----" + e.getCause() + " - " + e.getMessage());
        }
    }
    // Método Editar
    public void actualizarSW(Context context, int id, String nombre, String apellido, int telefono, String direccion, Response.Listener listener, Response.ErrorListener errorListener){
        this.context = context;
        try{
            String url = IPC_SERVER + "cliente_sw/editar.php?id_cliente="+id+"&nombre_cliente="+nombre+"&apellido_cliente="+apellido+"&telefono_cliente="+telefono+"&direccion_cliente="+direccion;
            requestQueue = Volley.newRequestQueue(context);
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, listener, errorListener);
            requestQueue.add(jsonObjectRequest);
        } catch ( Exception e){
            Toast.makeText(context, "ConflictoEditar: " + e.getMessage(), Toast.LENGTH_LONG).show();
            System.err.println("Editar: ----" + e.getCause() + " - " + e.getMessage());
        }
    }
}
