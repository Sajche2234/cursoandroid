package com.edutec.webservicelocal;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.edutec.webservicelocal.complementos.CleinteVO;
import com.edutec.webservicelocal.complementos.MetodosSW;

import org.json.JSONArray;
import org.json.JSONObject;

public class ActualizarActivitySW extends AppCompatActivity implements Response.Listener<JSONObject>, Response.ErrorListener {
    EditText edtEditarCliente, edtNombreCliente, edtApellidoCliente, edtTelefonoCliente, edtDireccionCliente;
    MetodosSW metodosSW = new MetodosSW();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actualizar_sw);
        edtEditarCliente = findViewById(R.id.edtEditarCliente);
        edtNombreCliente = findViewById(R.id.edtNombreCliente);
        edtApellidoCliente = findViewById(R.id.edtApellidoCliente);
        edtTelefonoCliente = findViewById(R.id.edtTelefonoCliente);
        edtDireccionCliente = findViewById(R.id.edtDireccionCliente);
    }

    public void onClick(View view) {
        switch (view.getId()){
            case R.id.idImgEditar:
                this.buscarID();
                break;
            case R.id.btnEditarCli:
                this.actualizarC();
                break;
        }
    }
    private void buscarID(){
        String buscar = edtEditarCliente.getText().toString();
        if(!buscar.isEmpty()){
            metodosSW.buscarDIWS(this, Integer.parseInt(buscar), this, this);
        } else {
            Toast.makeText(this, "Debe de llenar el campo", Toast.LENGTH_LONG).show();
        }
    }

    private void actualizarC(){
        String buscar = edtEditarCliente.getText().toString();
        String strNombre, strApellido, strTelefono, strDireccion;
        strNombre = edtNombreCliente.getText().toString();
        strApellido = edtApellidoCliente.getText().toString();
        strTelefono = edtTelefonoCliente.getText().toString();
        strDireccion = edtDireccionCliente.getText().toString();
        System.out.println(buscar + "........");
        if(!buscar.isEmpty() && !strNombre.isEmpty() && !strApellido.isEmpty() && !strTelefono.isEmpty() && !strDireccion.isEmpty()){
            metodosSW.actualizarSW(this, Integer.parseInt(buscar), strNombre, strApellido, Integer.parseInt(strTelefono), strDireccion, this, this);
            Toast.makeText(this, "Datos Actualizados correctamente.", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Debe ingresar un codigo.", Toast.LENGTH_LONG).show();
        }
    }

    private void resultadoBusqueda(JSONObject response){
        CleinteVO clienteVO = new CleinteVO();
        JSONArray jsonArray = response.optJSONArray("tbl_cliente");
        try {
            JSONObject jsonObject = jsonArray.getJSONObject(0);
            System.out.println(jsonObject);
            clienteVO.setId_cliente(jsonObject.optInt("id_cliente"));
            clienteVO.setNombre_cliente(jsonObject.optString("nombre_cliente"));
            clienteVO.setApellido_cliente(jsonObject.optString("apellido_cliente"));
            clienteVO.setTelefono_cliente(jsonObject.optInt("telefono_cliente"));
            clienteVO.setDireccion_cliente(jsonObject.optString("direccion_cliente"));

            edtNombreCliente.setText(clienteVO.getNombre_cliente());
            edtApellidoCliente.setText(clienteVO.getApellido_cliente());
            edtTelefonoCliente.setText(String.valueOf(clienteVO.getTelefono_cliente()));
            edtDireccionCliente.setText(clienteVO.getDireccion_cliente());
        } catch (Exception e){
            Toast.makeText(this, "Error referente a Editar...", Toast.LENGTH_LONG).show();
            System.err.println("Ed------ " + e.getCause() + " ----- " + e.getMessage());
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        this.resultadoBusqueda(response);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(this, "Error al response Eliminar datos. " + error, Toast.LENGTH_LONG).show();
        edtNombreCliente.setText("");
        edtApellidoCliente.setText("");
        edtTelefonoCliente.setText("");
        edtDireccionCliente.setText("");
    }
    // https://www.flaticon.com/
}