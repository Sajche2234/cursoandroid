package com.edutec.clase202106241;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private ListView lista;
    ArrayList<String> vocales = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Declarar o onocializar los componentes
        lista = findViewById(R.id.listaVocales);
        // Llenar la lista.
        vocales.add("LETRA A");
        vocales.add("LETRA E");
        vocales.add("LETRA I");
        vocales.add("LETRA O");
        vocales.add("LETRA U");
    }
}