package com.edutec.myappbdsqlite;

import androidx.appcompat.app.AppCompatActivity;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.edutec.myappbdsqlite.complementos.ConstantesSQL;

public class InsertarActivity extends AppCompatActivity {
    EditText edtNombre, edtSabor, edtTipo, edtPrecio;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insertar);
        edtNombre = findViewById(R.id.edtNombre);
        edtSabor = findViewById(R.id.edtSabor);
        edtTipo = findViewById(R.id.edtTipo);
        edtPrecio = findViewById(R.id.edtPrecio);
    }

    public void onClick(View view) {
        this.insertarBebida();
    }

    private void insertarBebida(){
        String nombre = edtNombre.getText().toString();
        String sabor = edtSabor.getText().toString();
        String tipo = edtTipo.getText().toString();
        String precio = edtPrecio.getText().toString();
        if(!edtNombre.getText().toString().isEmpty() &&
                !edtSabor.getText().toString().isEmpty() &&
                !edtTipo.getText().toString().isEmpty() &&
                !edtPrecio.getText().toString().isEmpty()){
            ConectorSQLite conectorSQLite = new ConectorSQLite(this, ConstantesSQL.BD_BEBIDA, null, ConstantesSQL.VERSION);
            SQLiteDatabase database = conectorSQLite.getWritableDatabase();
            // Creación del query para la inserción de los datos.
            try {
                String consultaInsertar;
                consultaInsertar = "INSERT INTO " + ConstantesSQL.TABLA_BEBIDA + " ("+ConstantesSQL.CAMPO_NOMBRE+", "+ConstantesSQL.CAMPO_SABOR+", "+ConstantesSQL.CAMPO_TIPO+"" +
                        ", "+ConstantesSQL.CAMPO_PRECIO+") VALUES ('"+nombre+"', '"+sabor+"', '"+tipo+"', "+precio+");";
                // Acción de la consulta
                database.execSQL(consultaInsertar);
                database.close();
                edtNombre.setText("");
                edtSabor.setText("");
                edtTipo.setText("");
                edtPrecio.setText("");
            } catch (Exception e){
                e.getMessage();
            }
        } else {
            Toast.makeText(this, "Debe de llenar todos los campos", Toast.LENGTH_LONG).show();
        }
    }
}