package com.edutec.myappbdsqlite;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.edutec.myappbdsqlite.complementos.ConstantesSQL;

public class BuscarActivity extends AppCompatActivity {
    private EditText edtBuscarBebida;
    private TextView txtNombre, txtSabor, txtTipo, txtPrecio;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar);
        edtBuscarBebida = findViewById(R.id.edtBuscarBebida);
        txtNombre = findViewById(R.id.txtNombre);
        txtSabor = findViewById(R.id.txtSabor);
        txtTipo = findViewById(R.id.txtTipo);
        txtPrecio = findViewById(R.id.txtPrecio);
    }

    public void onClick(View view) {
        this.buscarBebida();
    }
    private void buscarBebida(){
        ConectorSQLite conectorSQLite = new ConectorSQLite(this, ConstantesSQL.BD_BEBIDA, null, ConstantesSQL.VERSION); // Es la variable de conexión
        SQLiteDatabase database = conectorSQLite.getReadableDatabase();
        String[] parametro = { edtBuscarBebida.getText().toString() };
        if(!edtBuscarBebida.getText().toString().isEmpty()){
           try {
               // Constula por ID
               String consultaID;
               consultaID = "SELECT " + ConstantesSQL.CAMPO_NOMBRE + ", " + ConstantesSQL.CAMPO_SABOR + ", " + ConstantesSQL.CAMPO_TIPO + ", " + ConstantesSQL.CAMPO_PRECIO + " FROM " +
                       ConstantesSQL.TABLA_BEBIDA + " WHERE " + ConstantesSQL.CAMPO_ID + " = ?;";
               Cursor cursor = database.rawQuery(consultaID, parametro); // Consulta mas parámetro.
               cursor.moveToFirst();
               txtNombre.setText(cursor.getString(0));
               txtSabor.setText(cursor.getString(1));
               txtTipo.setText(cursor.getString(2));
               txtPrecio.setText(cursor.getString(3));
               cursor.close();
           } catch (Exception e){
               e.getMessage();
           }
        } else {
            Toast.makeText(this, "Debe de ingresar un código en el campo de texto.", Toast.LENGTH_LONG).show();
        }
    }
}