package com.edutec.myappbdsqlite;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.edutec.myappbdsqlite.complementos.ConstantesSQL;

public class ActualizarActivity extends AppCompatActivity {
    EditText edtBuscar, edtNombre, edtSabor, edtTipo, edtPrecio;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actualizar);
        edtBuscar = findViewById(R.id.edtBuscarBebidaAc);
        edtNombre = findViewById(R.id.edtNombreAc);
        edtSabor = findViewById(R.id.edtSaborAc);
        edtTipo = findViewById(R.id.edtTipoAc);
        edtPrecio = findViewById(R.id.edtPrecioAc);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBuscarBebidaAc:
                this.consultarID();
                break;
            case R.id.btnActualizarBebidaAc:
                this.actualizarBebida();
                break;
        }
    }
    private void consultarID(){
        if(!edtBuscar.getText().toString().isEmpty()){
            ConectorSQLite conectorSQLite = new ConectorSQLite(this, ConstantesSQL.BD_BEBIDA, null, ConstantesSQL.VERSION);
            SQLiteDatabase database = conectorSQLite.getReadableDatabase();
            String[] parametro = { edtBuscar.getText().toString() };
            try {
                String query= "SELECT * FROM " + ConstantesSQL.TABLA_BEBIDA + " WHERE " + ConstantesSQL.CAMPO_ID + " =?;";
                Cursor cursor = database.rawQuery(query, parametro);
                cursor.moveToFirst();
                edtNombre.setText(cursor.getString(1));
                edtSabor.setText(cursor.getString(2));
                edtTipo.setText(cursor.getString(3));
                edtPrecio.setText(cursor.getString(4));
                cursor.close();
            } catch (Exception e){
                e.getMessage();
            }
        } else {
            Toast.makeText(this, "Debe ingresar el dato a buscar", Toast.LENGTH_LONG).show();
        }
    }
    private void actualizarBebida(){
        String nombre, sabor, tipo, id, precio;
        nombre = edtNombre.getText().toString();
        sabor = edtSabor.getText().toString();
        tipo = edtTipo.getText().toString();
        id = edtBuscar.getText().toString();
        precio = edtPrecio.getText().toString();
        if(!nombre.isEmpty() && !sabor.isEmpty() && !tipo.isEmpty() && !precio.isEmpty()){
            ConectorSQLite conectorSQLite = new ConectorSQLite(this, ConstantesSQL.BD_BEBIDA, null, ConstantesSQL.VERSION);
            SQLiteDatabase database = conectorSQLite.getWritableDatabase();
            String query = "UPDATE " + ConstantesSQL.TABLA_BEBIDA + " SET " +
                    ConstantesSQL.CAMPO_NOMBRE + " = '" + nombre + "', " +
                    ConstantesSQL.CAMPO_SABOR + " = '" + sabor + "', " +
                    ConstantesSQL.CAMPO_TIPO + " = '" + tipo + "', " +
                    ConstantesSQL.CAMPO_PRECIO + " = " + precio + " WHERE " +
                    ConstantesSQL.CAMPO_ID + " = " + id + ";";
            database.execSQL(query);
            database.close();
            Toast.makeText(this, "Datos actualizados correctamente", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Debe comletar el formulario.", Toast.LENGTH_LONG).show();
        }
    }
}