package com.edutec.actividad202106291;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class AdaptadorLista extends BaseAdapter {
    // Crear una variable del tipo contexto.
    private Context context;
    // Va a hacer una referencia a una lista
    private int referenciaLista;
    // Crear el array list
    private ArrayList<String> dato = new ArrayList<>();
    private ArrayList<Integer> num = new ArrayList<>();

    // Referencia del contructor para las referencias del parametro.
    public AdaptadorLista(Context context, int referenciaLista, ArrayList<String> dato, ArrayList<Integer> num){
        this.context = context;
        this.referenciaLista = referenciaLista;
        this.dato = dato;
        this.num = num;
    }
    @Override
    public int getCount() {
        // Mide el tamaño de la vista
        // El retorno es el valor o tamaño del array list que se definio.
        return this.dato.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Se va a crear la estructura de la información que va a llevar.
        // El INFLATER, replica algo que ya existe a algo nuevo o personalizado.
        // Se va a crear una vatiable del tipo VISTA, que sera el retorno.
        View v = convertView;
        LayoutInflater layoutInflater = LayoutInflater.from(this.context); // Reestructurar un contexto, y aplicarlo a algo referenciado, en un layout.
        v = layoutInflater.inflate(R.layout.lista_personalizada, null);
        String mes;
        int numero;
        mes = this.dato.get(position);
        numero = this.num.get(position);
        TextView campo;
        TextView numerocampo;
        campo = v.findViewById(R.id.txtLista);
        numerocampo = v.findViewById(R.id.txtNum);
        campo.setText(String.valueOf(mes));
        numerocampo.setText(String.valueOf(numero));
        return v;
    }
}
